function gen_Bethe(generations, neighbors) {
    var nodes = [];
    var edges = [];
    var counter = -1;

    // Recursive function to create a new node and call itself to generate
    // the branches(subtree) structure of the graph
    function tree(level, neighbors, head) {
        counter++;
        nodes.push({
            id: counter,
            label: String(counter),
            group:level
            });
        var current_node = counter;
        if (head != current_node) {
            edges.push({
                from: head,
                to: current_node
            });
        }

        if (level == 0) {
            return 0;
        }

        var childs = neighbors - 1;
        // Only the first/central node is special. For the recursive call
        // it has all neighbors
        if (current_node == 0) {
            childs = neighbors;
        }

        var low = level - 1;
        for (var i = 0; i < childs; i++) {
            tree(low, neighbors, current_node);
        }

        return 0;
    }

    tree(generations, neighbors, 0);

    return {
        nodes: nodes,
        edges: edges
    };
}

function gen_dimerBethe(generations, neighbors) {
    var nodes = [];
    var edges = [];
    var counter = -1;

    function tree(level, neighbors, head) {
        counter++;
        var current_node = counter;
        // first node
        nodes.push({
            id: counter,
            label: String(counter),
            group:20
        });
        // second node
        counter++;
        nodes.push({
            id: counter,
            label: String(counter),
            group:100
        });
        // link the two nodes to build the dimer
        edges.push({
            from: counter-1,
            to: counter,
            dashes: true
        });

        if (head != current_node) {
            edges.push({from: head, to: current_node, arrows:'to'});
            // The second lattice
            edges.push({from: head+1, to: current_node+1, arrows:'to'});
        }


        if (level == 0) {
            return 0;
        }

        var childs = neighbors - 1;
        if (current_node == 0) {
            childs = neighbors;
        }

        var low = level - 1;
        for (var i = 0; i < childs; i++) {
            tree(low, neighbors, current_node);
        }

        return 0;
    }

    tree(generations, neighbors, 0);

    return {
        nodes: nodes,
        edges: edges
    };
}
