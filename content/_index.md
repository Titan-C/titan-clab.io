+++
title = "My turf on the internet"
author = ["Dr. Óscar Nájera"]
draft = false
images = ["images/back.jpg"]
+++

> You're here because you know something. What you know you can't explain,
> but you feel it. You've felt it your entire life, that there's something
> wrong with the world.
>
> --- Morpheus, The Matrix
