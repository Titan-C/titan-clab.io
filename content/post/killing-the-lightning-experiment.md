+++
title = "Killing the Bitcoin Lightning experiment"
authors = ["oscar"]
date = 2025-01-17
tags = ["lightning", "bitcoin"]
categories = ["thoughts"]
draft = false
images = ["images/posts/lightning.jpg"]
[caption]
  text = "Photo by Johannes Plenio on Unsplash"
  url = "https://unsplash.com/photos/photo-of-island-and-thunder-E-Zuyev2XWo"
+++

A while ago, I got all excited around the idea of [Bitcoin as art]({{< relref "bitcoin-is-art" >}}). It was such a
satisfying perspective that I decided to embrace it and implement a tipping
service on the Bitcoin Testnet for this website. A fun experiment, a way to
learn something and tinker around with the technology. The best part was: it
somehow didn't require any maintenance. It would work without further tinkering.

Sure, from time to time I would need to reboot my server and I had to spend the
extra effort to restart the Bitcoin Testnet node, lightning node and wallet and
test again that everything still worked and it did.

The network itself however didn't keep up with such stability and started facing
some issues. The test network of the time `testnet3` not having a strict
difficulty adjustment suffers under vandalism[^fn:1]. Storms of new
blocks flood the chain, which my server manages well enough, and with node
pruning it doesn't consume much disk space. The service could still run fine and
I let it be, yet it was a sign of the upcoming problems.


## Why kill it? {#why-kill-it}

The test network is for testing, with coins that have no value and which should
be easy to get. That is no longer true. I didn't really need more coins myself,
I had already my channels open. But **you** do need them to jump into this
experiment. My personally recommended website for starting with lightning on the
testnet [htlc.me](https://htlc.me) doesn't work anymore. The website is there, it looks like it
should work but it doesn't, all payment and invoices fail. Getting new test
coins on chain is not free anymore[^fn:1], they have now accrued value
and a price to get. This seems an inevitable consequence of a Proof of Work
network. Since it costs resources to run, it demands a market for fees and
market for coins. It seems fair to charge something for using its resources. Yet
that makes it harder to get started, as bitcoin developers claim.

The bitcoin developers decided to reset the Test network, give everybody a new
testing ground and fix some of the problems with the current test network. I
agree with that. The problem is that the new network to my knowledge does not
yet offer the same services the previous does. I can run a node on the new
`testnet4`, yet it has not yet embraced lightning. To this day, supporting
`testnet4` is still a feature request for [LND](https://github.com/lightningnetwork/lnd/issues/8966), the lightning implementation I've
been playing with. The service I used to find lightning nodes [1ml.com](https://1ml.com/) has no
information about `testnet4` at all. [mempool.space](https://mempool.space) already provides block
exploring services for `testnet4`, yet it doesn't do it for lightning. I get the
again the feeling that lightning doesn't yet run on `testnet4`, and worse that
people don't care about it.

All that leaves me to conclude, that this technology got left behind, that I'm
discouraged to work on this technology. It feels like trying to play with a
broken toy no one wants to fix. I continue to believe in Bitcoin as art, and
that its technology with lightning have potential, yet it wont be part of the
future as a technology if its stewards can't keep it operational, inclusive and
user-friendly.

It makes no sense to me to continue operating a test node on the lagging
`testnet3`, and I can't really migrate to `testnet4`, because it doesn't work
for lightning yet. It has been months since it launched, and there is still no
consideration for it yet. So much for the lies, that crypto moves at neck
breaking pace and it is the future of finance.

Whatever the Bitcoin _valuation_ is, it remains completely detached from
reality. Well I guess all these traded assets(securities, real state, government
debt) are, they exist for speculation. I just happen to have spent more effort
understanding Bitcoin and I can't see reason to think it is the future, when it
isn't accessible for everybody to play with.


## But it is an asset, you shouldn't be playing with it! {#but-it-is-an-asset-you-shouldn-t-be-playing-with-it}

I disagree. We have monopoly, to play with money and buying assets. Crypto is an
asset and a payment network, and we need to be able to play with it without the
risk. That consideration of course doesn't go well with the crypto people, who
want to dump their bags of coins on the rest of the world. Thus they only focus
on offering the _real_ thing and you must buy into that.

I believe lightning is a wonderful technology. Today, it is however impossible
to play with it, it doesn't run on `testnet4`. You can spin it locally on the
regtest, yet the whole point of a payment network, is the network, not the
chain. There it fails, and makes it for now to operate a lightning node. I wish,
that the ecosystem improves at some time in the future and to be able to restart
this project.

Or, maybe, I'll end up fully embracing the nature of a Proof of Work chain and
relaunch the service on the mainnet. For that I still need to develop some more
confidence to have a service running with real money on the internet. I remain
quite risk adverse on that front.

[^fn:1]: <https://blog.lopp.net/griefing-bitcoin-testnet/>
