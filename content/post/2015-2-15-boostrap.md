---
title:  Using a Framework for web development
authors: ["oscar"]
date: 2015-02-15
categories: [webdesign]
---

This comes after an old idea, that has been bugging me in the last years: "I
can't do everything myself anymore, there is just no time for such thing".
I'm not a specialist in everything, I chose to focus long ago in physics.
Thus, for all those web development things I can't do them on my own anymore. I
have to use framework and templates.

This is a general Idea. Just take whatever is already done and use it to build
upon it. When it fails go to its
groundings, but don't redo its hole simplified structure because it takes to
much time and I'm not getting paid for it, nor does my value increase a lot
for knowing how something is done that nobody knows but uses more efficiently
than me. Yes knowing is good but doing is better. It is actually what gets
paid.

So this time again a web redesign. This time using a Framework. Why didn't
I use it before? It is just so simple and makes things work. Well, indeed I still
have to learn its features, but I don't have to bother anymore about its
inner workings. More amazingly, it solves a bunch of problems, I did not
even imagined would occur in web development. Just take the current knowledge
and its solutions to build upon
them. This is the case of this web design, building responsive websites.
Curious change, from websites of fixed layouts to completely dynamical in size.

So where came the idea of using a framework. Well it was even there, long
before I even though I couldn't do everything myself. But this time was more
influenced as I saw many websites behaving really well and using this framework
[Bootstrap]. Well, I was also strongly biased by the previous technologies I presented
in the last post like [SASS] and [Font Awesome].

I must confess that CSS is still a big weakness of mine. Nevertheless using
templates for the website and a few custom changes, I got a new website I'm now
quite pleased to have.

[Font Awesome]: http://fontawesome.io/
[Bootstrap]: http://getbootstrap.com/
[SASS]: http://sass-lang.com/install
