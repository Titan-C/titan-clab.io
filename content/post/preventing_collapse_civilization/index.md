+++
title = "Preventing the collapse of civilization"
authors = ["oscar"]
date = 2022-08-19
categories = ["thoughts"]
draft = false
images = ["images/posts/ruins.jpg"]
[caption]
  text = "Photo by Jeremy Bezanger on Unsplash"
  url = "https://unsplash.com/photos/nGxXsVcib8w"
+++

I stumbled upon this talk by Jonathan Blow on YouTube and I must share it.
Civilizations flourish thanks to the technologies they invent. Yet those same
technological achievements get lost because the same civilizations that created
them fail or at least they fail to propagate them to the future. We open
ourselves to those same vulnerabilities over and over again.

Today, our global civilization feels at the brink of collapse. Wars in many
places, one even including a nuclear super-power, global trade and supply chains
disrupted because of war and political restrictions due to COVID mandates, and
an upcoming debt crisis due to misallocation of arbitrary generated funds in
response to COVID. Jonathan Blow was not aware of these facts in his 2019 talk.
He wasn't predicting the civilization's failure but the failure to propagate our
knowledge. How our civilization might structurally collapse because of bad
software.  Software is the foundational technology of our era, and it isn't a
robust technology.

Software is buggy all the time, we blame economics for it. The market doesn't
pay for quality and we have gotten used to bad software. Yet a more frightening
though is that we have never seen quality, robust software. We don't know if
that is even possible with our current skill set.

Software is eating the world, yet despite our reliance on it, it is not
advancing.  We don't expect it to be robust nor safe, and software developers
productivity keeps dropping. We need ever larger teams and manufacture less.
Software is not a productivity booster anymore, we are adding too much
complication.  Nowadays we must keep track of many things in our minds to work
with software, despite its main goal to automate and free our minds for other
tasks. We burden our main work with trivial stuff trying to get around the
software we use.

The more complication we introduce the less likely we are to survive a disaster
or institutional decay. Communicating knowledge gets harder and loss is
inevitable.  We need to simplify the systems so that people can keep up with the
knowledge and improve upon it.

Humanity has lost a lot of knowledge, what makes you thing our computer
technology is likely to survive when most people are consumers of it and the
_custodians_ of it work nowadays in high levels of abstraction and
virtualization.  Removing complexity might be the right short-term strategy,
because we constantly suffer from the huge fragile systems.

I leave you with the video:

{{< youtube  ZSRHeXYDLko >}}
