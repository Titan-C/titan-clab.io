+++
title = "Me as of today"
authors = ["oscar"]
date = 2019-08-03
categories = ["personal"]
draft = false
+++

Hello everyone, I'm starting a new section of my life and I want to engage
on new projects. But before jumping into anything new, I want to review
where I stand and what I can tell you about myself.

<!--more-->

I was born in the city of Quito, in Ecuador. A city, up high in the
mountains of the south American Andes. I enjoyed sun every day, but never
extreme temperatures, what a nice weather it was over there. I had an
incredibly resourceful dad. He always encouraged me to stop thinking about
what something is and instead to focus on what can I make it do? That lead
to an incredible amount of broken toys, but it was also fuel for my
curiosity. I self-taught myself to program websites. Then, I was curious
about the universe, and ended up studying physics in college. How wonderful
were those years!

I moved to Paris, France, to continue my studies in physics. I did a
masters and even a PhD on it. But more challenging than my studies was
moving to another country were I knew no one. I didn't even speak the local
language back then. But this experience taught me, that I can restart my
life. That I can move to unknown places and more than survive, I could even
succeed in my goals. I did end very frustrated with the academic system,
and decided to start again another live. I moved again this time to Munich,
Germany. Because I am always happy to work with computers, I started to
work as a software developer. My employer provides technical support to our
own free software. Free as in Freedom respecting software.

I have other passions. I started dancing many years ago and have not
stopped. There were time periods when I even did it at a competitive
level. I'll return to the competitive environment and do it again. Until
then, it will be always fun to connect with new people through dance.

What comes next? Over the years I realized how important is to get your
message across. I recently joined Toastmasters, a worldwide organization
helping you develop public speaking skills.  I also want to share more
about me and share my work, knowledge and experiences, thus I join
steemit. Finally, I would love jumping into the entrepreneur life and
trying to make a company to change the world. You'll read about that too.

I leave you with a picture of me, on a boat trip in the south of
France. Always be willing to embark on a new travel and explore something
new.
