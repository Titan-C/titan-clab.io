function range(start, end, step) {
  return Array((end - start) / step)
    .fill()
    .map((_, idx) => start + step * idx);
}

function make_plots(imported_data, target_div) {
  let [start, end, step, ...rest] = imported_data["mem:ram-cached"][0];
  const timespan = range(start, end, step);

  let mem_data = [timespan]; // x-values (timestamps)
  imported_data["mem:ram-cached"].forEach((row) => mem_data.push(row.slice(3)));

  const cursorOpts = {
    lock: true,
    focus: {
      prox: 16,
    },
    sync: {
      key: "moo",
      setSeries: true,
    },
  };

  let opts_mem = {
    title: "Memory Usage",
    id: "mem",
    class: "plots",
    cursor: cursorOpts,
    tzDate: (ts) => uPlot.tzDate(new Date(ts * 1e3), "Europe/Berlin"),
    width: 355,
    height: 260,
    series: [
      {},
      {
        show: true,
        // in-legend display
        label: "RAM used",
        value: (self, rawValue) =>
          (rawValue / 1024 / 1024 / 1024).toFixed(2) + "GiB",

        // series style
        stroke: "green",
        width: 1,
        fill: "rgba(0, 200, 0, 0.3)",
      },
      {
        show: true,
        // in-legend display
        label: "RAM Cached",
        value: (self, rawValue) =>
          (rawValue / 1024 / 1024 / 1024).toFixed(2) + "GiB",

        // series style
        stroke: "red",
        width: 1,
        fill: "rgba(255, 0, 0, 0.3)",
      },
    ],
    axes: [
      {},
      {
        size: 80,
        values: (u, vals, space) =>
          vals.map((v) => (v / 1024 / 1024 / 1024).toFixed(2) + " GiB"),
      },
    ],
  };

  let uplot_mem = new uPlot(opts_mem, mem_data, target_div);

  let opts_cpu = {
    title: "CPU utilization",
    id: "cpu",
    class: "plots",
    cursor: cursorOpts,
    tzDate: (ts) => uPlot.tzDate(new Date(ts * 1e3), "Europe/Berlin"),
    width: 355,
    height: 260,
    series: [
      {},
      {
        show: true,
        // in-legend display
        label: "User",
        value: (self, rawValue) => rawValue.toFixed(2) + "%",

        // series style
        stroke: "green",
        fill: "rgba(0, 200, 0, 0.3)",
        width: 1,
      },
      {
        show: true,
        // in-legend display
        label: "I/O-wait",
        value: (self, rawValue) => rawValue.toFixed(2) + "%",

        // series style
        stroke: "red",
        fill: "rgba(255, 0, 0, 0.3)",
        width: 1,
      },
      {
        show: true,
        // in-legend display
        label: "system",
        value: (self, rawValue) => rawValue.toFixed(2) + "%",

        // series style
        stroke: "blue",
        width: 1,
        fill: "rgba(0, 0, 200, 0.3)",
      },
    ],
    axes: [
      {},
      {
        size: 80,
        values: (u, vals, space) => vals.map((v) => v.toFixed(0) + "%"),
      },
    ],
  };

  let cpu_data = [timespan]; // x-values (timestamps)
  imported_data["cpu:user-wait-system"].forEach((row) =>
    cpu_data.push(row.slice(3))
  );
  let uplot_cpu = new uPlot(opts_cpu, cpu_data, target_div);

  let opts_if = {
    title: "Network bandwidth",
    id: "network",
    class: "plots",
    cursor: cursorOpts,
    tzDate: (ts) => uPlot.tzDate(new Date(ts * 1e3), "Europe/Berlin"),
    width: 355,
    height: 260,
    series: [
      {},
      {
        show: true,
        // in-legend display
        label: "Input",
        value: (self, rawValue) =>
          ((8 * rawValue) / 1000 / 1000).toFixed(2) + "Mbit/s",

        // series style
        stroke: "green",
        fill: "rgba(0, 200, 0, 0.3)",
        width: 1,
      },
      {
        show: true,
        // in-legend display
        label: "Output",
        value: (self, rawValue) =>
          Math.abs((8 * rawValue) / 1000 / 1000).toFixed(2) + "Mbit/s",

        // series style
        stroke: "red",
        fill: "rgba(255, 0, 0, 0.3)",
        width: 1,
      },
    ],
    axes: [
      {},
      {
        size: 80,
        values: (u, vals, space) =>
          vals.map(
            (v) => Math.abs((8 * v) / 1000 / 1000).toFixed(0) + " Mbit/s"
          ),
      },
    ],
  };
  let if_data = [timespan]; // x-values (timestamps)
  imported_data["if:in-out"].forEach((row) => if_data.push(row.slice(3)));
  if_data[2] = if_data[2].map((x) => -x); // output below axis

  let uplot_if = new uPlot(opts_if, if_data, target_div);

  let opts_disk = {
    title: "Disk utilization",
    id: "disk",
    class: "plots",
    cursor: cursorOpts,
    tzDate: (ts) => uPlot.tzDate(new Date(ts * 1e3), "Europe/Berlin"),
    width: 355,
    height: 260,
    series: [
      {},
      {
        show: true,
        // in-legend display
        label: "Write",
        value: (self, rawValue) =>
          (rawValue / 1024 / 1024).toFixed(2) + "MiB/s",

        // series style
        stroke: "green",
        fill: "rgba(0, 200, 0, 0.3)",
        width: 1,
      },
      {
        show: true,
        // in-legend display
        label: "Read",
        value: (self, rawValue) =>
          Math.abs(rawValue / 1024 / 1024).toFixed(2) + "MiB/s",

        // series style
        stroke: "red",
        fill: "rgba(255, 0, 0, 0.3)",
        width: 1,
      },
    ],
    axes: [
      {},
      {
        size: 80,
        values: (u, vals, space) =>
          vals.map((v) => Math.abs(v / 1024 / 1024).toFixed(2) + " MiB/s"),
      },
    ],
  };
  let disk_data = [timespan]; // x-values (timestamps)
  disk_data.push(imported_data["disk:read-write"][1].slice(3)); // write goes first
  disk_data.push(imported_data["disk:read-write"][0].slice(3).map((x) => -x));

  let uplot_disk = new uPlot(opts_disk, disk_data, target_div);
}

fetch("monitor.json")
  .then((r) => r.json())
  .then((data) => make_plots(data, document.getElementById("plots-container")));
