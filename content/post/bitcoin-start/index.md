+++
title = "Getting started with bitcoin"
authors = ["oscar"]
date = 2020-12-16
tags = ["bitcoin"]
draft = false
highlight_languages = ["scheme"]
+++

Bitcoin is all over the news, this time with big investors pouring money
into it, a global pandemic that put the economy on hold, and governments
printing money like crazy. Price raises, it is a bubble again.

Maybe it is responsible this time to at least try to learn about Bitcoin
without rushing to get it like everybody else seems to be doing again. The
only way to learn about it, benefit from it and clear the doubts from the
healthy skepticism is to test the technology by yourself. This is my
experience setting up a node.

The most well known educator is this space seem to be Andreas Antonopoulos,
and he has kindly made his work available for everyone with an internet
connection. Over at that popular code sharing platform is his book
[Mastering Bitcoin](https://github.com/bitcoinbook/bitcoinbook). It is a nice and rigorous presentation of Bitcoin, and I
decided to start learning from that book.


## Downloading the bitcoin blockchain {#downloading-the-bitcoin-blockchain}

Right away from the start of the book you must download the entire Bitcoin
blockchain. I first wanted to download the test network, it was quite a
fast process on the VPS I booked for the task. Yet, I quickly realized that
the examples used on the book use the main network and I had to give it a
try. [Host during blockchain download](#host-during-blockchain-download) has a collection of graphs about what
was going on my system. Downloading the testnet happened from 4:30 pm to
6:05 pm 2020/12/11. During that time my system performed OK, yet as you can
see from the CPU utilization graph, a substantial amount of CPU time was
wasted waiting for IO. The system seems to have loaded all downloaded data
into the Memory cache and it didn't take enough space in RAM for the task
of indexing the blockchain. I read later on the Arch Linux wiki that during
the indexing phase I should increase in the settings the amount of
available, because it looks like the bitcoin daemon seems to have its
memory usage capped on the default settings. This fact becomes more evident
later on while downloading the main network. RAM usage seems to have an
upper bound.

The download mechanism seems stable, yet at 11am of the next day 2020/12/12
there is a sudden change of behavior. The CPU utilization on userspace
triples and the download network bandwidth drops to half. On the download
logs that time corresponds to blocks starting on 2020/03/29. That is
already this year. I'm yet ignorant if there was a big change since then
on, if there were just a lot more transactions in volume to deal with or
if that is the working part of the blockchain. Being that date so close
after the big market crash of this year, I do assume it was the movement of
people liquidating their bitcoin for cash. Now that money is on recent
addresses and there is a lot more of reconciliation work.


### Host during blockchain download {#host-during-blockchain-download}

<link href="/css/uPlot.min.css" rel="stylesheet" type="text/css">
<style type="text/css">
 .plots {
 display: inline-grid;
}
</style>
<script type="text/javascript" src="/js/uPlot.iife.min.js"></script>
<script type="text/javascript" src="plots.js"></script>
<div id="plots-container"></div>


## Closing remarks {#closing-remarks}

It was an interesting experience, and I value having a monitoring of my
host in place, otherwise this little insight I gained would have never been
possible. I can start with questions to explore as I go further on learning
about bitcoin.

It seems for now that to keep a node running you need about 2GiB of RAM, I
needed 368GiB of disk space for the main network and another 32GiB for the
test network. I have read that you can have a node that doesn't keep the
full history, I will try that later on. The CPU is high during the download
and indexing yet later on it is really low, however I still need to test
how it performs when doing queries.

The cost of running the node is very relative, I find that a VPS is cheap
enough these days, yet it is a new expense every month and that adds
up. Thus it becomes very relevant to be a participant in the network. On
the other hand, even if you don't. The fees banks charge nowadays are the
same, so running your "own bank" as many say cost you the same in fees,
only that you now need to put some extra work to manage your node. I'll see
where does this experiment take me.

It was also a fun experience to learn about [uPlot](https://github.com/leeoniya/uPlot) to draw the graphs. It is
a very small library, with just about the minimum to plot good looking time
series. It was simple to embed on this website and I'll most certainly use
it on the future.
