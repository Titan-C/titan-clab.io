+++
title = "A year on my cloud"
authors = ["oscar"]
date = 2018-05-04
tags = ["sovereign"]
categories = ["cloud"]
draft = false
[caption]
  text = "Photo by Sylwia Bartyzel on Unsplash"
  url = "https://unsplash.com/photos/tME8s001BNQ"
+++

It has been a very complicated year regarding my cloud services. I started
completely unaware of the difficulties, and it has been a fun and painful
ride. The fact that there are projects aiming to provide personally
controlled cloud services is great. The fact that there is people being
paid for doing this kind of work for companies means that it ain't that
simple to manage.

I encourage to try the [sovereign](https://github.com/sovereign/sovereign) project, which assists you in building and
maintaining your own private cloud. I even managed to contribute a bit to
it. That is the whole point of Free Software: Let others learn from your
work and experience and share with them the burden of managing a project
that serves the common good. Yes, there is a lot about Freedoms involved in
it too, and I may better say Open Source for the reduced scope in my last
sentence, but that is just a consequence of Free Software and I rather
promote with the full philosophy.

[Sovereign](https://github.com/sovereign/sovereign) covers all the basic services for your personal cloud, and many
more that I don't even use. I dislike that currently it is not modular
enough, because there were few things I just didn't want installed in my
server. In the end choosing not to install something reduces to commenting
out roles, but there is no way of uninstalling what you already deployed to
your server. That is just a step you have to manually do. However, this
encouraged me to learn about [Ansible](https://www.ansible.com/) and how to configure my own server
manually and with the configuration manager. In the end for someone like
me, I ended up winning.

I'm not yet completely out of mainstream providers, it is hard to get out,
but I'm way more conscious on my personal choices nowadays. I also feel
good that I'm trying to fight against the current state of affairs
regarding cloud services by using my server. The cloud is a good thing, but
you need to understand its business model. You need to understand why its
_"Free"_, and what you are giving away for using it.

Using your own cloud should be for everyone, and there are amazing Free
Software projects that help you to do so. Having your cloud does feel
really expensive because you need money to pay for the VPS provider plus
time to manage it, compared to free stuff as offered by mainstream
providers it just seems pointless and that's why so many of us have
followed the path of resignation and embrace the current business models
where we pay with our data. Instead, we should support Free Software
projects and companies that aim to protect your data and give you freedom
respecting internet. Yes, it will cost you cash, but you'll be aware of
what is going on. Also, the Free Software model help in distributing power,
there is no single _Super provider_ you can always choose the provider you
trust and do business with them. This helps avoid monopolies, because you
are decentralizing the use of a service.

My final thought is to give it a try. Yes, it can feel like a nightmare,
but is our duty to fight for a free and open internet.
