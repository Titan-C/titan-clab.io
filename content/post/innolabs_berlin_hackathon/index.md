+++
title = "Participating to my first hackathon"
authors = ["oscar"]
date = 2018-05-17
tags = ["hackathon", "healthcare"]
categories = ["events"]
draft = false
thumbnail_size = "225x150 bottom"
+++

I'm currently looking for my next life project and to figure it out I
trying a lot of stuff. To that goal I'm getting involved in as many things
as possible. Last week, I went to the [Innolabs healthcare hackathon in
Berlin](https://www.innolabs.io/hackathonBerlin.html). It was quite an awesome event and I learned a lot from it. I never
really worked on the health sector before, thus it was a nice introductory
experience.

The new world view I confronted myself with was: a capitalistic focus on
every matter. Every argument stars with: _"This is a huge market..."_ or
_"We are currently spending this much and we need a way to save on
this..."_. Although, this might not be specific to the health sector, but
more to the world out of academia where we just complain: _"there is no
funding for research"_. Truth be told again, nothing works without
funding. Or more explicit, nothing works without energy, but energy is not
for free and funding just makes it easier to get it.

The event was really fun. The organizers gave us a nice venue as workplace
and took care of the food. It is so good to be able to start your day not
thinking about breakfast and other meals. Given were a set of challenges to
focus on, nothing to specific since the aim was get you involved with any
area where help is needed. Afterwards, you can think a long while about
which your next step should be, your little next contribution that will
make a change in that area.

I met the cofounder of [Mentalab](https://mentalab.co/) at the hackathon. His project, as of today,
is in an early stage and his focus in the hackathon was to figure out use
cases for his device. He built the thing, but what is it good for? His
product lets you measure your brain activity, which scientist can only do
in labs. His goal is to allow you to do it too and at you home. What he
needs now is a user friendly way to show you what all those measurements
mean. You need them already processed in some way to understand what is
going on in your head.

By the end of the first day, and with the help of a lot of people that
joined the team, we came to decide that a great application of Mentalab's
product would be using it for a measured mental training, and specifically
for elderly people. They are the target group since in those late years
they might not get enough mental stimulation and they are prone to suffer
from Alzheimer's disease. Nowadays, we are in a terrible position because
there is no cure for it and taking care of people suffering from it costs a
lot of money. Therefore, we could be probably saving money and people's
lives with some preventive medicine. The medical doctors in our team said
the alternative treatment of music and meditation does seem to give a
positive effect on patients <sup id="f2aff122ca0f465df87336f1757d017c"><a href="#khalsa2015" title="Dharma Singh Khalsa, Stress, Meditation, and Alzheimer's Disease Prevention:  Where the Evidence Stands, {Journal of Alzheimer's Disease}, v(1), 1-12 (2015).">khalsa2015</a></sup><sup>,</sup><sup id="098d8a40aafe757f6df474a5d23c8152"><a href="#innes2017" title="Innes, Selfe, Khalsa \&amp; Kandati, Meditation and Music Improve Memory and Cognitive  Function in Adults With Subjective Cognitive Decline: a  Pilot Randomized Controlled Trial, {Journal of Alzheimer's Disease}, v(3), 899--916 (2017).">innes2017</a></sup><sup>,</sup><sup id="7e7851e951f58dfdce7ac738c9aa59b2"><a href="#russell-williams2018" title="Jesse Russell-Williams, Wafa Jaroudi, Tania Perich, Siobhan Hoscheidt, Mohamad El Haj \&amp; Ahmed Moustafa, Mindfulness and Meditation: Treating Cognitive Impairment  and Reducing Stress in Dementia, {Reviews in the Neurosciences}, v(0), nil (2018).">russell-williams2018</a></sup>.

Now we had a product to build in half a day. Impossible of course, there
is no over night success even less half a day success. Nevertheless the
point was to get the idea started and see how many people would buy into
it. Then we got our hands dirty, no more discussions. Build a presentation,
but a prototype online, iterate until time runs out. What we build? The
concept idea of a phone app that connects to Mentalab's brain activity
measuring device and gives you feedback on your meditation. Why? Because
your brain is a muscle, but it is inside of you, so you can't see it sweat,
nor grow, nor get stronger. You need the feedback.

After the presentations where over we got the 3<sup>rd</sup> price, and we where
very happy. This is of course only an incentive to keep working on this
project, and for Mentalab to focus more on this use case for their
product.

I'll leave you with the original notification of the winners, originally
displayed in [Linkedin](https://www.linkedin.com/feed/update/urn:li:activity:6399670512943005696).

# Bibliography
<a id="khalsa2015"></a>[khalsa2015] Dharma Singh Khalsa, Stress, Meditation, and Alzheimer's Disease Prevention:  Where the Evidence Stands, <i>Journal of Alzheimer's Disease</i>, <b>48(1)</b>, 1-12 (2015). <a href="http://dx.doi.org/10.3233/jad-142766">doi</a>. [↩](#f2aff122ca0f465df87336f1757d017c)

<a id="innes2017"></a>[innes2017] Innes, Selfe, Khalsa & Kandati, Meditation and Music Improve Memory and Cognitive  Function in Adults With Subjective Cognitive Decline: a  Pilot Randomized Controlled Trial, <i>Journal of Alzheimer's Disease</i>, <b>56(3)</b>, 899-916 (2017). <a href="http://dx.doi.org/10.3233/JAD-160867">doi</a>. [↩](#098d8a40aafe757f6df474a5d23c8152)

<a id="russell-williams2018"></a>[russell-williams2018] Jesse Russell-Williams, Wafa Jaroudi, Tania Perich, Siobhan Hoscheidt, Mohamad El Haj & Ahmed Moustafa, Mindfulness and Meditation: Treating Cognitive Impairment  and Reducing Stress in Dementia, <i>Reviews in the Neurosciences</i>, <b>0(0)</b>, nil (2018). <a href="http://dx.doi.org/10.1515/revneuro-2017-0066">doi</a>. [↩](#7e7851e951f58dfdce7ac738c9aa59b2)
