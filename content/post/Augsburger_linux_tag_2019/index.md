+++
title = "My talk at the 18. Augsburger Linux-Infotag 2019"
authors = ["oscar"]
date = 2019-07-07
tags = ["linux", "checkmk"]
categories = ["talks"]
draft = false
thumbnail_size = "320x88 center"
+++

I have always been a Free Software fan and I'm always happy to participate
in events where I meet like minded people. Linux days are a great
opportunity for this. They are super fun and still hold the free software
spirit about sharing your knowledge and learning from others. They are also
organized by volunteers, which deepens the nice feeling of community. I
found one of these events. The [18. Augsburger Linux-Infotag 2019](https://www.luga.de/Aktionen/LIT-2019/) would take
place in the neighboring city of [Augsburg in Germany](https://en.wikipedia.org/wiki/Augsburg). Since I was going
participate the company I work with proposed me if I could give a talk
about our product. Without giving much thought to it I just agreed. Dare
first to do the things then figure out the way.

There were many requirements for the talk that I underestimated. First, it
would have to be in German. German is not my mother language and I haven't
been practicing it enough to sustain a fluid speech. Second, although I
work on the software I was going to talk about, as a developer I don't
fully know how to use it. I instead have specialized knowledge on how some
components work internally so that I can change and extend them. The speech
was targeted at one of our consultants who completely knows the product.
He is a hobbyist beekeeper, who monitors his bee colonies with our
software. Despite my limitations, I just gave it a try, it should be fun.

Before going to the conference, I had a chat with our consultant to learn
what I should talk about. I received the slides prepared, thus I was
feeling extremely comfortable. But on the day of the presentation, well
that overconfidence was squashed by the fear of public speaking. This was
the first time I hold such a long talk and also the first time I even give
a talk in German. On top of that I got really stressed when looking the
faces of my audience show boredom and thus I just tried to speed things up.

The talk was recorded and just recently I dared to look at it. That is a
necessary step to learn from myself and look for improvement. I immediately
noticed that I didn't even speak in full sentences. I have a hard time
understanding myself because of that and because I constantly jump between
ideas. Although I tried to follow the slides, and had speech notes
prepared, I lacked a lot of practice to deliver the talk.

When the presentation was over, it was only half-way. There were a lot of
questions coming up, which gave me the impression more people were really
interested on the product more than on the curious beekeepers usage. It
felt great, despite their faces of boredom they liked what I told them, or
were at least interested enough on it to inquire about it. I answered the
questions to the best of my knowledge at that time, and now that I see the
video some questions I would answer differently today. They weren't
completely wrong, but for such a big software package that I'm constantly
learning about my point of view has changed and my knowledge of its
capabilities has grown.

I'll keep the video here for future reference. I hope to get better in
public speaking and have the skill to do it in any language that I
know.

{{< youtube OXTn0YFuwvE >}}
