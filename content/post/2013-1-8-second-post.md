---
title:  Second Post
authors: ["oscar"]
date:   2013-08-01
categories: [personal]
---

On my first post after blog opening, I offered to write as often as possible to record my learning and share ut. Unfortunately, even for me, it has not happened. This is the second post. That does not mean, I spend my time without getting new knowledge. It means instead that I still need to practice about this blogging task and discipline myself to it. Certainly the lack of monetary income hinders me from publishing. Hope to change this, and that it doesn't take other many months for the third post.

On a summary of what I have learned in the last months and suggested readings for you.
* Learn about the Python programming language and boost libraries to joint python and C++, especially for numerical applications.
* Enjoy the pleasure of typing in a Dvorak keyboard layout, it does make sense to swicht. I even created my custom keyboard layout, as a mixture of Dvorak or spanish, english and programmer Dvorak.
* I learned about the academic degrees in my country and how outdated they are, I lost the oportunity to do my PhD at ETH Zurich, just because my 6 years long Ecuadorian degree wasn't enough for admission, despite having found a PhD thesis supervisor. Someday I will help standardize my country's academic law, but for now I aim still at accomplishing my academic goals.
* Learned Lattice Boltzmann

On a more random subject, I danced the WDSF world championship standard in Melbourne Australia. All those years dedicated to dancing delivered and incredible reward. I travelled to Brazil and went to the Rio Carnival, very nice. But as personal recommendation, don't go there. It's just to crowded, one weak earlier is the best option: you get the nice weather and sunny beaches as well, but a lot less people, you can still watch the parade rehearsals for free, and it is far cheaper than during the holiday.

Until next time!!!
