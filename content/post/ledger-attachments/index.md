+++
title = "Attachment files in plain text accounting"
authors = ["oscar"]
date = 2020-11-13
tags = ["emacs"]
draft = false
[caption]
  text = "Photo by Michael Walter on Unsplash"
  url = "https://unsplash.com/photos/iJMitgqRaZ8"
+++

I have been using [ledger](https://www.ledger-cli.org) to keep track of my expenses. It is simple, it
doesn't interfere with my workflow, and I can edit data with my favorite
text editor Emacs, because ledger only deals with plain text files. I love
this workflow.

At some point I ran onto the limitation about how to organize the invoices
I receive. Most of the time they are pdfs I receive on my email, but it
could also be physical ones that I then _scan_ with my smartphone. I
started storing them on a local folder, renaming them into something I can
make sense when looking at all the files, and finally writing their
location as some extra metadata to the posting on my ledger file.

Naturally, after doing it a few times, my annoyance grew. I needed to
automate this process. Emacs is great to edit text and its operating system
capabilities should also take care of the renaming of the files. So I dived
into practicing some ELisp to implement a function that does exactly what I
need.

Emacs already has a mode for ledger files, which provides all the
functionalities to edit the files and each posting individually. The next
code block automates my previous manual workflow. I'll comment on each step
of this function, as a kind of reminder to myself what everything does.

```scheme
(defun on/ledger-link-invoice ()
  "Attach an invoice file to this posting."
  (interactive) ; This allow the function to be called with interactively; M-x
  ; when-let* is a beatiful macro, every language should have it. What it does
  ; is as a let bind variables, if that variable holds a true value it goes into
  ; the body of the macro. As soon as any varible evaluates to nil, the process
  ; stops and the body is not executed.
  (when-let* (; Use ledger-mode function to get the date of the posting
              (date (ledger-xact-date))
              ; Extract the payee of the posting and replace whitespace with underscores
              ; the trim right is because this extraction includes the new line \n
              (payee (replace-regexp-in-string " " "_" (string-trim-right (ledger-xact-payee))))
              ; Select the file I want to link in my file, default the folder of searching
              ; to ~/Downloads, "Attach: " is for the prompt
              (src-file (read-file-name "Attach: " "~/Downloads"))
              ; Create the new relative path "invoices/YYYY-MM-DD_payee.pdf"
              (file-name (concat "invoices/" date "_" payee "." (file-name-extension src-file))))
    ; move my selected file to the new location and new name given by file-name.
    ; The expansion is done for safety reasons to be sure which is the target
    ; base directory. I my case it does match to whery the ledger file is
    ; and the relative path is good enough
    (rename-file src-file (expand-file-name file-name "~/accounting"))
    ; this is the text editing part. Move to the very-begging of the posting
    (ledger-navigate-beginning-of-xact)
    ; move pointer to end
    (end-of-line)
    ; This writes a new line on my buffer
    (newline)
    ; write the metadata string "    ; Invoice: invoices/YYYY-MM-DD_payee.pdf"
    (insert "    ; Invoice: " file-name)))
```

That is all the automation I need for the moment. I'm happy that I can
adapt Emacs to help me on these tasks. Now every digital invoice is neatly
organized and won't get lost. There is now enough context, for each posting,
because I link to the invoice.
