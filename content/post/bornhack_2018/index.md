+++
title = "Bornhack 2018"
authors = ["oscar"]
date = 2018-08-26
tags = ["hackercamp"]
categories = ["events"]
draft = false
album = "bornhack2018"
+++

How do you take your summer holidays? Some seek rest by laying down the sun
like an iguana warming up their blood. Just doing nothing to compensate for
the hard work over the year. I'm not against that, I do it too. But I found
a new alternative, you can go out and learn something new. Only a year ago
I discovered hacker camps, an outdoor tent camp event for hackers and
makers.

It is really hard to disconnect from my precious tech gadgets, I just take
them everywhere with me. Holidays might be for disconnecting, but the real
deal when it comes to rest is change of activity and environment. This is
precisely why a hacker camp is so awesome. Now I can finally enjoy my
contact to nature, sleeping on a tent, change my life routine and do things
differently and still enjoy the benefits and distractions of modern day
technology that I'm so fond of.

But outdoors? Is there even WiFi? Well, there is WiFi and it's fast. Speeds
you can only get at hacker camps, because we know how important it truly
is, not the slow connection you get in your holiday resort or Café (maybe
that's intentional to get you out). The wired(yes it's still a thing) and
wireless networks are put together by passionate professionals, who take
pride of setting up the camp with full internet connectivity. And they do a
fantastic job. You can see the presentation of their work [on the network
for Bornhack 2018](https://www.youtube.com/watch?v=-7AgLD5yKRs).

But it is a Hacker camp, am I safe? Of course not! But you are not any
safer in your favorite Café either, you have to take care of yourself, keep
your firewall on at all times, go to the security talks to learn more and
keep learning. On the other hand, you could be safer than in your favorite
Café. These are networks build for hackers, they put the effort to enforce
encryption in communications. But participants also try to hack them all
the time. Here it is a game not a crime, so keep in mind you can win or
loose, but you can also hit the restart button and play again. Remember to
always have a backup of your data, and make sure it works.

Hacking is a display of creativity, a philosophy that glorifies what is fun
and clever. Hacking is the philosophy of exploration, personal expression,
pushing boundaries, and breaking the rules. That's why you can expect to
see a lot of crazy stuff all over the place and try to hack on them
too. The lights in the camp are connected to the internet and you can
change the patterns they show. The bar runs live statistics on the drinks
being ordered and displays those statistics to you. You get a badge which
you are expected to hack on and the organizers even help you getting
started. I also took part in a workshop about bleaching my clothing. For
all the years it takes to wear out some shirts until they become
comfortable, this extra bleaching now made them more amazing and worth
wearing for some extra years.

This event is surprisingly well priced. You get your camping space + fast
internet + electricity + electronic badge ready to be hacked + great talks
about hacker relevant topics and a community of amazing people for the
price of an average camping space. This is because the event runs on the
pure joy and voluntary work of the organizers making it possible. You
should not see this as a service you purchase, even if you consider this
holidays; but as a community you join and thus you are expected to
contribute back, be awesome to everyone, give a helping hand to everyone
and the organizers and share your knowledge.

Maybe we'll meet there next year. Have a look at the event website <https://bornhack.dk>
