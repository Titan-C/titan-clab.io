+++
title = "Org-CV an Org export backend for CV"
authors = ["oscar"]
date = 2018-05-06
tags = ["emacs", "org-mode"]
draft = false
projects = ["org-cv"]
[caption]
  text = "Photo by Álvaro Serrano on Unsplash"
  url = "https://unsplash.com/photos/hjwKMkehBco"
+++

I'm again in this time of my life where I'm looking for a new long term
project to work on. Because optimization is my favorite way of
procrastination, instead of just making a Curriculum Vitae (CV) for job
applications, I decided that I need to implement an org-mode export
backend. This backend will let me keep my CV in `org-mode` and then export
it to \\(\LaTeX\\) or markdown for my website. This exporter now lives in
gitlab as [org-cv](https://gitlab.com/Titan-C/org-cv) and the documentation is [here](https://titan-c.gitlab.io/org-cv/).

I work with org-mode, because it is amazing, because it already provides
exporters and its syntax allows to store information beyond the plain
prose. That is by the way the key, an item inside a CV includes information
beyond what I have done, but where? when? and with whom? Such information
can be encoded in a [property drawer](https://orgmode.org/manual/Property-syntax.html), and the exporter's job is to
understand them so that they can be displayed later on in a pertinent
location. With this in mind I'll describe a job entry like this:

```org
* Employement
** One job
:PROPERTIES:
:CV_ENV: cventry
:FROM:     <2014-09-01>
:TO:     <2017-12-07>
:LOCATION: a city, a country
:EMPLOYER: The employer
:END:
- I think about awesome stuff to do.
- I do the awesome stuff I think of.
- I write about awesome stuff I do.
```

I use `org-mode's` tree hierarchy: a heading about `Employement` and then
the subtrees are the job positions taken. The `:CV_ENV: cventry` is the
`key-value` pair that makes this the special CV item, the other ones are
self explanatory and provide the context information of the
entry/job. Finally, I can use org-mode to write about the work done.

In a CV, its header includes a bunch of contact information, thus many new
keywords are recognized by the exported to this purpose. The example below
shows their availability and use.

```org
#+TITLE: My dream job
#+AUTHOR: John Doe

#+ADDRESS: My Awesome palace
#+ADDRESS: Fantastic city -- Planet Earth
#+MOBILE: (+9) 87654321
#+HOMEPAGE: example.com
#+GITHUB: John-D
#+GITLAB: John-D
#+LINKEDIN: John-Doe
#+PHOTO: smile.png
```

After you have written your information you can rely on my package [org-cv](https://gitlab.com/Titan-C/org-cv)
to generate the \\(\LaTeX\\) file for your pdf. I currently support
`moderncv` and `altacv` templates. I also wrote an extension for `ox-hugo`
so that I can prepare a markdown file, which HUGO will understand to
generate my Online CV which.
