+++
title = "Upgrading this blog again"
authors = ["oscar"]
date = 2023-11-01
categories = ["webdesign"]
draft = false
images = ["images/posts/hulk.jpg"]
thumbnail_size = "150x150 smart webp"
[caption]
  text = "Photo by Gabriel Tovar on Unsplash"
  url = "https://unsplash.com/photos/green-and-black-action-figure-oTKanDGugaA"
+++

Web development is the skill that irritates me the most. It has improved over
the years. The tools available today are far superior. But, I only glimpse at
better web design skills. I often times struggle to understand why some websites
look the way they do. Even with full access to their source code, replicating
them isn't always evident. There are `HTML` &amp; `CSS` tricks I refuse to practice
in the aim of simplifying what I imitate. That means some layouts don't look
exactly the same.

Years ago, I promised myself not to waste effort trying to do the webdesign by
myself. And yet this blog's design is a topic on itself. One I have worked on
too many times. It is a personal character trait: _optimization is my
favorite procrastination_.

Which was the trigger this time? Since I moved to [Hugo](https://gohugo.io/), I have been happy thanks
to their well-organized themes gallery. It offers a lot of attractive designs.
Most of the time, I find something far better than anything I could produce
myself. Still, there is a undeniable irritation in my mind, when I use someone
else's design to express myself. I started with the [`hugo-tranquilpeak-theme`](https://github.com/kakawait/hugo-tranquilpeak-theme),
which was an excellent design for a blog. Yet, it then fell short with my
motives. This led me to the `hugo-academic-theme` now [`Wowchemy`](https://wowchemy.com/). It was a
better fit and it served me well over the years, until it felt bloated.


## It is too much {#it-is-too-much}

`Wowchemy` served first academics, then research groups too, then almost
everybody else. The creator found success and patronage in this evolution. But
all those new responsibilities bloated `Wowchemy` beyond my taste. And for some
reason I couldn't resist the urge to update. I could not leave it frozen in the
version, which I was already satisfied with.

The updates were always painful, I somehow could not find the update guides. In
the author's aim to simplify installation and serve new users, I felt he ruined
updates for old users. Every new internal design choice annoyed me more. Focused
on serving his new users, but not aligned with my preferences.

The site remained fast and well optimized, but it bloated. Too much stuff, too
much JavaScript. Too many special configurations in the front matter of every
section or post. It solved for layouts through configuration options. For
certain structured information like the CV items, it was the only way, but I
didn't like it. Anything I wanted, I had to adhere to the author's prescription.
It wasn't setup the way I wanted.


## If you want it your way, you do it yourself! {#if-you-want-it-your-way-you-do-it-yourself}

My anger accumulated until I wanted to give it a try and do it myself. I remain
inspired by `Wowchemy`'s academic layout, but I aimed for a minimalist and
simple design. I prefer to reuse elements as much as possible, and make
everything feel almost the same.

It turns out, simple and minimal is an elusive goal. I could cut out a lot of
`JavaScript`, but not eradicate it. Some functionality like rendering \\(\LaTeX\\)
formulas, and the testnet bitcoin tip box, demand `JavaScript`.  Finally,
~~although not yet implemented,~~ the search functionality.

`CSS` styling is still a nightmare, but since I discovered [Tachyons](https://tachyons.io/) it has been
a lot more manageable. I rely on their examples, which are minimal and
effective. I'm well served by [FontAwesome](https://fontawesome.com/) and [Academicons](https://jpswalsh.github.io/academicons/) for the icons.

With those dependencies, I can now build each page as I like. The internal
`HTML` is a lot simpler, I limit the amount of `div` containers I use. I make
the effort to style everything with `tachyons` and reduce the custom `CSS`. It
continues to be a steep learning process. There were moments where I struggled
trying to achieve my desired alignment and look. It turns out some `div` are
necessary to get some effects. Many other times it is my lack of skill that
prevents me from reaching the page design I aspire.

I'm currently happy with the result. I eliminated most of the front matter
configuration. That makes it easy to write my content in `org-mode` and export
it to markdown, which `Hugo` understands best. I use `org` properties for fine
control over the exporter, particularly on the about page. There I use [org-cv](https://titan-c.gitlab.io/org-cv/),
which I designed to manage all the content in a single org file. Then export it
with \\(\LaTeX\\) to pdfs and to markdown for this website.  I can only achieve
that consistency by doing everything myself. I'm surprised how far I could get
with `CSS` alone. I can arrange the layout within `CSS` and minimal nested
structures of `HTML`, that makes it a lot simpler.


## Did I solve my issues? {#did-i-solve-my-issues}

Pretty much, I'm over scratching my itch. I have a simple website and I can work
again on the content. I don't have the update problem, because I'm the updater.
I decide when to update this site, based on my needs and bug fixes. I have my
own interest in focus. A 3rd party theme instead tries to solve many problems
for its users, which raises the complexity. I have less of a documentation
problem, because the theme is small enough to fit in my head. Most of the layout
is content based not structured data, which makes it much easier to relearn or
adapt.

But I had to make so many design choices I did not foresee, and I had to face
certain realities. I can't get rid of `JavaScript`. It was as always more work
than I imagined, but now I know a little more what I'm doing.
