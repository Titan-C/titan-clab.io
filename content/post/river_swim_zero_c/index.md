+++
title = "Why swim in a river at 0°C?"
authors = ["oscar"]
date = 2021-01-09
categories = ["personal"]
draft = false
+++

There are plenty of reasons: Get out of your comfort zone, build character, discover your limits. There are plenty of justifications: Simply crazy, settling a bet, religious penitence/purification.

All those might apply, however for me it was a CELEBRATION. A celebration of how well I'm doing nowadays. I was born in a big city, I grew up surrounded by the grayness of concrete. The river in my city was so polluted that already its smell would keep you away. Going into nature implied a family trip, implied planning, implied getting everyone to agree and then traveling far away until we could reach a clean river, a waterfall a lake. Only then could I jump into those waters, and I did no matter how cold they were.

Today, I live in a different city. Today, I can wake up, look at the sunny day and choose to go out. Today, I find a river after a short bike ride and do the same, jump in. Today, I am living the LUXURIES OF MY CHILDHOOD.

Your mindset is certainly everything. To me, this was a milestone, a celebration, a luxury. Thus, the was no moment of hesitation, no instant where my body shivers at the cold nor a smile drops of my face.

{{< youtube w1HBmIhDHxs >}}
