+++
title = "Posts"
authors = ["oscar"]
draft = false
weight = 10
images = ["images/back.jpg"]
[cascade]
  image_size = "768x768 webp"
  author = ["Dr. Óscar Nájera"]
  thumbnail_size = "225x150 webp"
  metadata = true
+++
