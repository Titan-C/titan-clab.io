+++
title = "Forgetting - Our filter for the irrelevant"
authors = ["oscar"]
date = 2018-07-31
categories = ["thoughts"]
draft = false
[caption]
  text = "Photo by Davide Cantelli on Unsplash"
  url = "https://unsplash.com/photos/r0q06hjTgOc"
+++

I just stumbled upon this article: [To Remember, the Brain Must Actively
Forget | Quanta Magazine](https://www.quantamagazine.org/to-remember-the-brain-must-actively-forget-20180724/), and it served me as quite a confirmation bias. I
always reflected why it is so hard to learn something, or remember
something. Consider, what a great super power it would be to remember it
all. The true new point of view for me here is the merit of forgetting.

> If we remembered everything, we would be completely inefficient because our
> brains would always be swamped with superfluous memories.
>
> --- Oliver Hardt

This is so obvious that it was easy to miss for me. If every memory was
popping up all the time, I would have a hard time focusing. Everything
would be in front of me and I would confront a massive information overload.

> Forgetting serves as a filter. It filters out the stuff that the brain
> deems unimportant.
>
> --- Oliver Hardt

This is certainly the only option we have. Placing filters to the
unimportant. Filtering out everything that consumed our attention for a
while, but is not meant to last. This raises a new question in my mind.

**Is our current information overloaded life responsible for making it hard to learn things or remember things?**

We are bombarding our brain with so much information. And by the nature of
"social media", "the news", "advertising", everything is presented in the
most attractive way, but in the end is irrelevant. Thus, my brain would get
used to judge almost everything as irrelevant, because that is the true
case.

Would filtering my information input in advance help me bring balance to my
brain's own filtering process? Would it give me the change to remember more
things?
