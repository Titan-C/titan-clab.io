+++
title = "American Physical Society March Meeting 2017"
authors = ["oscar"]
date = 2017-03-20
tags = ["physics"]
categories = ["talks"]
draft = false
+++

This March I had the opportunity to go to the American Physical Society
March Meeting in New Orleans. Since everyone gets a time slot for a talk, I
got my chance too. It was scheduled too early and I didn't enjoy a big
audience which lowered my stress levels. It was the first day so that I
could enjoy the rest of my time talking with other people instead of
stressing about my talk.

The slides are contained here

<div style="padding-bottom: 80%;position:relative;">
<iframe src="https://titan-c.github.io/MM2017/" width="100%" height="100%" style="position:absolute;"></iframe>
</div>

You can find the full screen version of the presentation at:

<https://titan-c.github.io/MM2017/>
