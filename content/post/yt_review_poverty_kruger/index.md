+++
title = "How To Escape Poverty? - Is Your Thinking Keeping You Poor?"
authors = ["oscar"]
date = 2020-04-19
tags = ["review", "entrepreneur"]
categories = ["selfdev"]
draft = false
[caption]
  text = "Photo by Razvan Chisu on Unsplash"
  url = "https://unsplash.com/photos/Ua-agENjmI4"
+++

I stumbled upon this talk by Douglas Kruger on YouTube. I decided to take
notes and write my own summary to deepen the learning experience. It
provides me with a reference for later times. Douglas Kruger mentions 8
points, I reorganized them into 5 themes.

<!--more-->


## The Quest {#the-quest}

The starting point to get you where you want to be is to see a learning
curve separating you and your goal, not an abyss. You are not separated
from your goals by a number of years. You are separated from your goals by
a number of actions. There you have control.

Give yourself permission to try and fail. Let it be OK that others laugh at
you.


## The Mindset {#the-mindset}

You'll never be wealthy if you hate the rich. Money is neither evil, nor
embarrassing, nor finite. Wealth makes you an amplified version of
yourself, thus be sure to like who you are.

You must be willing to talk about money, and to figure out how to make more
of it. Money is infinite, because work adds value and thus money to the
system. The more we do, the more wealth there is for all. Grow the pool
instead of trying to contain it!


## About Positioning {#about-positioning}

Positioning determines your pay-scale, not the quality of your work. If you
are the celebrity of your industry, you are going to out earn the
non-celebrity. Be the icon, be the face, be the creative thinker.

Knowledge is important, but you have to bring personality, you have to
bring humanity. Bring things that can’t be automated. Embrace being a human
being with a face, a voice, ideas &amp; opinions. Anything that can be
commoditized becomes invaluable.


## Your Focus {#your-focus}

Invest most of your energy to generate income, release yourself from
the bottom line. Saving is important. However, saving means you'll starve
to death slower, if you forget to generate income.

You must act on your ideas. They can multiply your income 10x.  Risk
aversion may paralyze you. Yet, it is unsafer to rely on someone else to
take care of you, because you are not their priority. Job security doesn't
exist, you exchange work with somebody for a salary. Generate it yourself
from your ideas! Allow yourself to fail, and keep trying. The average
millionaire has been bankrupt 3 times. Risk is there, but reward too.


## Leave the ranks of the poor {#leave-the-ranks-of-the-poor}

If Jeff Bezos, Bill Gates, or any other billionaire were left in your
current situation, would they become rich again? For sure! Would they act
like you now?

Knowledge and education are out there, you must embrace it. You need to
move out of the spaces that hold you down, because they depress your
thinking. You get averaged by your environment, thus get exposed to
successful individuals. They are worth the approach. The world is at your
service. Ask questions! Get the experience. Ask people with knowledge,
never remain silent.


## Watch the video {#watch-the-video}

{{< youtube  _ruRiTeX1jE >}}
