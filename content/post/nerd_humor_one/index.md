+++
title = "Nerd Humor: English - The torture of the conqueror"
authors = ["oscar"]
date = 2019-11-29
tags = ["toastmasters"]
categories = ["talks"]
draft = false
+++

On November 16<sup>th</sup> 2019, I held a short humorous speech at the [Wordstock
festival](https://web.archive.org/web/20191128232347/https://www.toastmasters-bayern.org/en/wordstock-festival/) in Nuremberg-Germany. I never imagined getting this far, and now I
only see it as the beginning. In humor and comedy, I have always been on
the spectator side. However, this year I chose to develop myself in public
speaking and it indeed has been a long way. It all started as a personal
challenge. I began by signing up in my public speaking club [Isar-Speak](https://isar-speak.com/).

Over the year I held some speeches and managed to spark some laughter in my
fellow club members. When my club officers advertised the Wordstock
festival, I was curious to go there. When they told me about be humorous
speeches, I thought it would be fun. When they invited us to give a
humorous speech, I didn't think I could do it. My first reaction was fear
and avoidance. However, advertising continued in my club and at some point
it does get into your head. They convinced me and I signed up to try to
test my humor. To ensure the quality of humorous speeches at the event a
selection procedure was of course in place. I had zero expectations on
getting anywhere beyond the first attempt.

I did not even want to move forward in the selection process, a nerdy topic
should be handicap enough. Thus, I decided to talk about the pain of
learning English. That is what many comedians do. They find something they
can complain about, and rant on it. English is quite relatable. We need to
learn it in this globalized world, and once we do, we forget how hard and
inconsistent it is. Bringing this inconsistencies back to your attention
should be funny.

The first time I tried the speech. I barely got any laughter out of
it. However, I was the best in a crowd of one. No one else in my club found
enough time to write a speech on their own. Thus, I was now the club
champion and would continue in the qualifying process. Now, I was
responsible for making my speech better.

For my second round, I actually had competition other club champions where
there. I wasn't the most loved on that evening, what a
relief. Nevertheless, the funniest one was busy and would not be able to go
to Wordstock to share his humor. I was the most average one among the
humorist. In a crowd of 3 persons that placed me second and thus I was the
next one given the option to continue. Oh, more responsibility! It is
beginner's luck, it has gone to far! I already qualified for the
Festival. This is a serious deal now. Could I rewrite my speech for a third
time. Sure!

I was invited to practice again in my club and test my third draft. It had
improved indeed. Despite the familiar content, they laughed more this
time. How can this be? 3 rounds and it starts to feel like good and fun
content? What is the golden secret to improve? _FEEDBACK!_ Every time I
shared my speech, laughter might have been scarce. But I always received
comments about it, some good other bad, but all friendly. I had more
information to go over the speech once again.

The video that follows is the fourth version of my speech, as presented at
the Wordstock event. I am amazed how far I have come. Yet, I was still
nervous on that day. I made a mistake right at the start mixing English and
Spanish, now _fixed_ on the video. It certainly confused my audience a lot
right from the start. I still lack awareness of what I say. I didn't
realize my mistake until I watched the Video. The speech was quite fluid. I
received quite some laughter from the audience.

{{< youtube gdy5ioVN1Sc >}}

The event was a show case without winners and rankings. Nevertheless, there
was gold for everyone. The true gold: Feedback from the audience. Lots of
written comments and congratulation cards. That is what you see on the
picture heading this post.

I'm re-posting this entry in my [steemit blog](https://steemit.com/dtube/@titan-c/nerd-humor-english-the-torture-of-the-conqueror).
