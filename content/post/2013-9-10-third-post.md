---
title:  Third Post
authors: ["oscar"]
date:   2013-09-10
categories: [personal]
---

I'll aim at not counting mi posts for future releases of content. But for now, since this look more like biographical notes of my life I can't find better titles for the posts. It's also because I'm not packing just one subject but some history of my live in the last time.

Now for the first improvement y manage to bring myself to writing this post only after a month. And in the last time many things have changed. I dedicated to configuring my house(my parents) network and home office. I got to set up a server and let it run [owncloud](http://owncloud.org/), for backup as the primary usage.

But most importantly now I live in Cachan a town in south Paris. After the bad luck that crossed my path with my PhD in Switzerland getting blocked for degrees equivalences, I managed to recover a position as a second year master student at the École Normale Supérieure de Cachan. And although the bureaucratic system here in horrible, and the other students as well as me are struggling to get properly matriculated and get the student card. It's a great experience to be here. I "learned" super fast french before my arrival, I felt it quite easy since it's the fourth language I'll be speaking. Nevertheless, one thing is in class, other thing is the real life here. But just after a week I fell also I'm doing really better, even though french is not the main language I'm speaking here.

Until next time!!
