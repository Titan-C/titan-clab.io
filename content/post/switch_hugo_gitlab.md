+++
title = "Moving to Hugo and GitLab pages"
authors = ["oscar"]
date = 2017-11-01
categories = ["webdesign"]
draft = false
+++

I'm guilty of having this online blog and not writing on it. I confess that
working on its looks and internals is way more fun for me than actually writing
some words in plain English for everyone to read. That however misses the point,
because it is the blog post that justify the blog, not its internals.

This website has experienced few visual updates over the years. Each time I
decide to embrace new technologies(at least to me) and to work on top of
somebody else's work instead of trying again to do the design by myself. Today
is a mayor step forward as I complete use a freely available theme instead of
just using a web-framework for design my website. I can still read about the
inner workings of the template, but I don't need to know everything.


## What inspired this change? {#what-inspired-this-change}

The idea of a static website is great and GitHub pages was offering a good
environment for hosting my website. Except that:

-   GitHub pages is only compatible with Jekyll
-   Your custom domain cannot use HTTPS

[Jekyll](https://jekyllrb.com/) is not bad at all. I did find it quite an useful tool. However, I
always found people complaining about it, and I wasn't always very happy
with it. When I started looking for alternatives, my first tendency was to
use tools written in Python. Python is a great language, I work with it all
the time. I found [Pelican](https://blog.getpelican.com/), [Nikola](https://getnikola.com/), they both seem to do the job. But they
have one problem, the themes. I'm not a web-designer, and I decided to just
stop trying to make my website completely from scratch. Pelican, Nikola,
and even Jekyll didn't offer a large set of themes. Maybe they do, I just
felt the offer was very limited and not what I was looking for.

This is where [Hugo](https://gohugo.io/), came into the game. I had found references about it
quite some times in web blogs. Its mayor advantage being speed, and it does
seem to be true (although I don't have a large website). I also found out
that they offer native support for org files. Since I migrated to Emacs, I
just love to work on org files. Hugo also offers a nice gallery of themes,
I was fond of quite a few theme propositions. Finally, Hugo is written in
Go, I don't know it, but learning about something new is always good
motivation and I decided to give Hugo a try.

I could host my new Hugo generated blog in GitHub, it is just pushing the
static website to the repo. But I have been quite angry at GitHub and their
services. For sure they host the largest amount of open source projects,
and offer many tools to manage them, but they don't seem to be moving
forward in their services. On the other hand [GitLab](https://about.gitlab.com/), being community driven
seems to favor much more the needs of developers and to integrate their
ideas into their services. Their web interphase does feel slower, and I
have not yet collaborated in a project inside GitLab. But their integrated
CI service is just fantastic. I can just make my docker images for my test
environment and let the CI run on those, even better I can host the CI
server myself and profit of a fantastic speedup in build times of my
personal little projects in comparison to having them on TravisCI or the
even better CircleCI. Also debugging my own docker image and GitLabCI
environments has been by far the easiest compared to TravisCI(close to a
nightmare) or CircleCI(quite fine because can SSH into CI server).

How about [GitLab Pages](https://about.gitlab.com/features/pages/)? It's great, you can use anything to build your
webpage. You just use you custom CI environment and deploy the output. They
offer plenty of [examples](https://gitlab.com/groups/pages) on how to use their service for large variety of
website generators. You can do anything, you are not constrained to using a
specific tool at a specific version with just a set of extensions. If you
don't like their prepared build environments, you can just do your own and
work with it. Finally, you can use your custom domain and TLS certificate,
you can use Let's encrypt for this. GitHub falls short on the last one and
having an HTTPS website is very important to me.


## How was it? {#how-was-it}

Installing Hugo was extremely easy on my computer, it is available in the
Archlinux repository. Just by following the quick start guide for Hugo. I
could experience how it works and tryout many of their available
themes. Having examples is probably the most powerful documentation.
I settled for the [hugo-tranquilpeak-theme](https://github.com/kakawait/hugo-tranquilpeak-theme), and started looking for an image
for the background, It is now a photo I took from the train stop. I like
the view of trees, rail roads and a green light.

Although there is an Hugo importer from Jekyll, and I did use it, I
preferred to redo the import by hand. I don't have many pages anyway. I was
easier that way as I ended up editing all the files anyway. Yes, I did the
work twice. I first performed and import into a test directory where I
would try Hugo. When I was happy about using this technology I did the
import again inside my git repository containing my website. I just feel it
is a better way to go, it is the repository holding my online presence and
it makes sense for me now to keep the same repository, in spite of the fact
that I'm changing my hosting provider and the technology that generates the
site.

How about the killer feature of supporting org-mode files. Well I didn't
like it and chose not to use it. I did not settle for markdown either,
org-mode in Emacs is just fantastic and I cannot let go. I preferred the
workflow offered by the org exporter [ox-hugo](https://github.com/kaushalmodi/ox-hugo). I feel it provides a more
intuitive way to write into my blog. Have can have a big org file with all
the posts organized inside sub-trees and use org tags to define Hugo's
categories and tags. Then export the sub-tree corresponding to a post into
a markdown file that Hugo will process. Version control becomes replicated
as I version control the source org file and the exported markdown, but I
guess it's just fine.


### Getting Let's Encrypt to work {#getting-let-s-encrypt-to-work}

This step included some guessing as the GitLab tutorial is focus on Jekyll
website. But using Hugo did not increased the difficulty.  I followed the
instructions in their [blog post](https://about.gitlab.com/2016/04/11/tutorial-securing-your-gitlab-pages-with-tls-and-letsencrypt/) with some variations. I first install
certbot directly from Archlinux package manager.

```sh
sudo pacman -S certbot certbot-apache
```

Because I do not want to run this as root, I don't need it anyway since I'm
doing the process manually, I create a folder where I can save the Let's
Encrypt data `letsencrypt_certs`, and then call certbot for my domain.

```sh
mkdir letsencrypt_certs
certbot certonly -a manual -d blog.example.com --work-dir letsencrypt_certs --config-dir letsencrypt_certs --logs-dir letsencrypt_certs
```

When I need to make the website available, I just save in the required HTML
file in the `static` folder with the sub-folder structure as given by Let's
Encrypt, which Hugo copies identically into the website.

`certbot` completes the certificate generation after the sites are
online. Finally paste into gitlab _Certificate (PEM)_ the `fullchain.pem` file
and into _Key (PEM)_ the `privkey.pem`.


## Comments {#comments}

<div class="outline-2 comment">

### Diego Zamboni - 2018-07-06T09:48:34Z {#diego-zamboni-2018-07-06t09-48-34z}

Hi Oscar - very interesting article. I found your website through ox-hugo's
[examples page](https://ox-hugo.scripter.co/doc/examples/), and this article got me thinking about HTTPS access for [my
website](https://zzamboni.org), which is hosted on Github Pages. Happily (and for reference of
anyone who reads this), I discovered that GH pages now supports [SSL
certificates for custom domains as well](https://blog.github.com/2018-05-01-github-pages-custom-domains-https/)! It was painless to set up, I
mainly had to update my DNS entries (which were very old) and change
"baseURL" in my Hugo site config to use my website's HTTPS URL, and
rebuild. GitHub even automatically generated and installed the Let's
Encrypt certificate for my site.

One more clarification - you say "GitHub pages is only compatible with
Jekyll", which is not correct. GitHub Pages will happily host any static
content, so a Hugo-generated website can be hosted without problems. I have
it configured from [my repository's /docs folder](https://blog.github.com/2016-08-17-simpler-github-pages-publishing/), so I just point Hugo's
publishDir to that same directory, and the whole process is painless.

The one thing I have not set up yet is CI. I still do this manually using a
simple script (which basically runs hugo, git add, git push), so I'm still
keeping duplicated content (actually triplicated: my ox-hugo source file,
the Markdown files it generates, and Hugo's output in /docs). I should
probably look into cleaning this up :)

Great website by the way!

<div class="outline-3 comment">

#### Óscar Nájera - 2018-07-07T00:28:19Z {#óscar-nájera-2018-07-07t00-28-19z}

Hi Diego, thanks for your comments.

Yes, Github nowadays does support SSL on their pages service, although the
last person I talked to about it didn't have a smooth experience. I'm glad
to hear is better now.

It is true that Github hosts static content and I should have said "Github
pages offers only to build Jekyll static sites". Nevertheless, I want to
emphasize the developer experience and flexibility I get out of Gitlab. You
get to do your own setup. CI is integrated directly to the project and to
deployment. As you mention content is triplicated for you, mine is only
duplicated for now. Getting CI to publish your website using Travis-CI is
hell(I couldn't make it when I tried), using Circle-CI is less of a hell(I
did manage to get Sphinx-Gallery docs to be generated and published from
there), Gitlab made it really easy for me.

<div class="outline-4 comment">

##### Diego Zamboni - 2018-07-08T18:35:37Z {#diego-zamboni-2018-07-08t18-35-37z}

Hi Oscar, thanks for your reply. I haven't played with CI, but I will take
your experiences in consideration. I have never seriously used GitLab, and
I didn't even know GitLab Pages existed. It's good to have different
options :)

</div>

<div class="outline-4 comment">

##### Diego Zamboni - 2018-08-09T11:33:34Z {#diego-zamboni-2018-08-09t11-33-34z}

Hi Oscar,

Just a quick follow up comment: this week I migrated both my websites
(<https://zzamboni.org/> and <https://cf-learn.info/>) from GitHub Pages to
Netlify. It was really easy because they support Hugo natively, and they
take care of CI, SSL certificates, and even DNS and comment forms if you
want, and it's all free. I'm very happy with it. Just thought I would
mention it in case you want to take a look.

</div>

</div>

</div>

<div class="outline-2 comment">

### Kaushal Modi - 2017-12-08T18:11:39Z {#kaushal-modi-2017-12-08t18-11-39z}

About "Version control becomes replicated as I version control the source
org file and the exported Markdown, but I guess it’s just fine."

It probably doesn't have to be that way :)

If you are **not** using the `lastmod` variable from the front-matter, you can use a Makefile to export from Org to markdown for Hugo, and then run hugo, on the fly in the Gitlab CI. Just call the `make` rule instead of `hugo` directly in the Gitlab CI setup.

Here are few examples:

-   `make vcheck doc_md hugo_doc` from [this Makefile](https://github.com/kaushalmodi/ox-hugo/blob/master/Makefile) generates
    <https://ox-hugo.scripter.co/> directly from [ox-hugo-manual.org](https://raw.githubusercontent.com/kaushalmodi/ox-hugo/master/doc/ox-hugo-manual.org) using
    _Netlify_.

-   `make doc` from [this Makefile](https://github.com/jwiegley/use-package/blob/master/doc/Makefie) generates <https://jwiegley.github.io/use-package/> directly
    from [use-package.org](https://raw.githubusercontent.com/jwiegley/use-package/master/use-package.org) using _Gitlab Pages_.

With that setup, you only need to commit the Org files (no committing of
Markdown or HTML). Granted that this will be a bit slower, because now the
CI is exporting all your posts from Org to Markdown each time, but it's not
slow by a huge factor. Hugo is still generating the whole site (pages,
taxonomies, etc.).

Hope that helps :)

_PS: As a side-note, I wished Disqus supported Org mode, or at least Markdown :)_

<div class="outline-3 comment">

#### Oscar Najera - 2017-12-11T12:24:16Z {#oscar-najera-2017-12-11t12-24-16z}

Thank you for the extra info and for ox-hugo. For the moment I still find
to much work to install emacs in the hugo docker image from gitlab then to
use a makefile to parse all posts in the org file and then let hugo build
the website.

Version controlling the output markdown file is easier. Also in the case
the export is not perfect I can still modify it manually and keep track of
those changes.

<div class="outline-4 comment">

##### Kaushal Modi - 2017-12-11T13:40:50Z {#kaushal-modi-2017-12-11t13-40-50z}

Hello Óscar, the referenced Makefile simply downloads a GNU/Linux 64-bit
built emacs binary and uses that (it doesn't deal with docker or
anything). But of course, using the Makefile is bit of an extra step and
the flow of committing the Markdown files is a fine flow too.

But I got surprised that you need to manually modify the Markdown files. My
aim of ox-hugo is to have the blog content 100% reproducible from just the
Org files.. no user intervention should be needed once the export is
done. Please file issues on the ox-hugo repo for the cases where you need
to manually edit the Markdown files. I'll see if I can fix those cases (or
at least document the limitations which I failed to see).

</div>

</div>

</div>
