---
title: The right dictionary
date:   2017-01-15
categories: ["personal"]
tags: ["writing"]
---

I just found a post on
using
[the right dictionary](http://mbork.pl/2017-01-14_I'm_now_using_the_right_dictionary) and
I immediately had to review its
reference:
[You’re probably using the wrong dictionary by James Somers][jsomers]. I
don't regret it. The core message is that we shall not only use
dictionaries to find the meaning of words we don't know, but to use
them to enrich our prose. I never considered there would be such a
dictionary that allows for such kind task. [James][jsomers] presents
the Webster's Revised Unabridged Dictionary, unfortunately that link
is broken. Nevertheless he provides a link to download
the [dictionary][webster], it a standard format for dictionaries.

After unzipping the file there is a second file that
needs to be expanded `stardict-dictd-web1913-2.4.2.tar.bz2`, that one
contains the dictionary. To use it the GoldenDict package can be
used. Detailed instructions for configuring it are [here][installdict]



[jsomers]: http://jsomers.net/blog/dictionary "You’re probably using the wrong dictionary"

[webster]: https://s3.amazonaws.com/jsomers/dictionary.zip

[installdict]: http://eduardosanchez.me/2015/09/07/installing-websters-revised-unabridged-dictionary-on-ubuntu-gnulinux/ "Installing Webster’s Revised Unabridged Dictionary on Ubuntu GNU/Linux"
