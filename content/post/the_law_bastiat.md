+++
title = "The Law by Frédéric Bastiat"
authors = ["oscar"]
date = 2023-12-22
categories = ["books"]
draft = false
images = ["images/posts/bastiat-the-law.webp"]
thumbnail_size = "150x150 smart"
slug = "bastiat-the-law"
+++

There is nothing like the filter of time. When a book from 1850 receives praise,
you know its insight is timeless and its quality undeniable. This essay
discusses what the law should be and shows the tragedy of what it has become.
The content still relevant today, reminds us of how the more things change, the
more they stay the same.


## What is the law? {#what-is-the-law}

It is the collective organization of the individual right to his defense.
Because every man has the right to defend, even by force, his person, his
liberty, and his property, a group of men have the right to organize a common
force to provide for this defense.

The collective right comes from the individual right. Since an individual cannot
lawfully touch the person, liberty, or property of another individual; the
common force cannot be lawfully used to destroy the person, the liberty, or the
property of individuals.

The law does not impose upon people it negates, it restricts people from doing
harm. The law violates neither the personality, nor the liberty, nor the
property of people. It only guards the personality, the liberty, the property of
others. It defends the equal rights of all.

When the law through the medium of its necessary agent, force, imposes anything;
it is no longer negative. It acts positively upon men. It enforces the will of
the legislator over the people's will. People cease to be men, they lose their
personality, their liberty, and their property.


## What perverts the law? {#what-perverts-the-law}

Labor being in itself a pain, and man being naturally inclined to avoid pain, it
follows that whenever plunder is less burdensome than labor, it prevails.
Because the law cannot exist without the support of a preponderant force, which
is in the hands of the legislator, and the tendency to plunder lives in the
heart of men, **the law perverts**. Instead of being a check upon injustice, it
becomes its most invincible instrument. The law becomes the tool of every kind
of avarice, instead of being its check!

Under the pretense of organization, regulation, protection, or encouragement,
the law takes from one party to give to another. It grabs the wealth acquired by
all the classes to increase that of one class. In this case, there is no class
which may not try, and with reason, to place its hands upon the law. As long as
we allow the law to divert from its true mission, and admit that it may violate
property instead of securing it, everybody will want to manufacture laws. Either
to defend himself against plunder or to organize it for his profit.


## Our misguided demands of the law {#our-misguided-demands-of-the-law}

The prejudice of our time is that it isn't enough that the law be just, it must
be philanthropic. It is not sufficient that it guarantees to every citizen the
free and inoffensive exercise of his faculties, applied to his physical,
intellectual, and moral development. It demands to extend well-being,
instruction, and morality directly over the nation.

These two missions of the law contradict each other. A citizen cannot at the
same time be free and not free. Fraternity goes together with voluntary.
Fraternity can't be legally enforced, without liberty being legally destroyed
and justice legally trampled under the foot. Legal plunder has two roots: one of
them is human greed, and the other is in misconceived philanthropy.

How is it that the strange idea of making the law produce what it does not
contain -- prosperity, wealth, science, religion -- should ever have gained
ground in the political world?


## What should we demand? {#what-should-we-demand}

The union of all liberties, the liberty of conscience, of education, of
association, of the press, of movement, of labor, and exchange. In other words,
the free exercise, **for all**, of all the inoffensive faculties. The destruction
of all despotism, even of legal despotism, and the reduction of law to its only
rational sphere, which is to regulate the individual right of legitimate
defense, or to repress injustice.

It is not the mission of the law to regulate our consciences, our ideas, our
will, our education, our sentiments, our works, our exchanges, our gifts, our
enjoyments. Its mission is to prevent the rights of one from interfering with
those of another, in any one of these things.

It is a fascinating read, and well worth spending an evening to go over this
essay. Yet as with any great work, you'll invest more time revisiting it and
your notes time and again. You can get the book from your favorite dealer. Mine
is downloading it from [Project Gutenberg](https://www.gutenberg.org/ebooks/44800).
