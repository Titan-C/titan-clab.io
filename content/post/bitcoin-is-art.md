+++
title = "Bitcoin is art"
authors = ["oscar"]
date = 2023-03-24
tags = ["bitcoin"]
categories = ["thoughts"]
draft = false
images = ["images/posts/bitcoin-is-art.webp"]
[caption]
  text = "Bitcoin on water lilies like Monet by Midjourney"
+++

Bitcoin is this strange thing I don't fully understand. Going down the rabbit
hole, as it is commonly known, I generally feel disappointed. Every _bitcoiner_
I meet is a fanatic insisting that I **buy bitcoin** instead of a supportive
mentor. The best buys of my life took place under guidance not pressure, thus
I'm always reluctant to listen. Why are they constantly trying to sell it to
you?

Bitcoin without being a company has more salesmen than utility.  Maybe there is
something there, maybe there isn't. A bubble that pops and comes back deserves
some analysis. [Saifedean Ammous'](https://saifedean.com/) Book: _The Bitcoin Standard_ is a mandatory
read. It helps you see why this magic internet money might be something else
than a bad deal. Yet, I confess that my first change of heart arose after
reading [Jeff Booth](https://www.jeffreybooth.com/)'s Book: _The Price of Tomorrow_.  It wasn't the
honest/hard/non-confiscable money argument that got me. It was the argument
about repricing the system. The need to let people reprice debt because writing
the debt off is as unlikely as paying it off is.

At my current knowledge and experience level, Bitcoin's advertised economic
promise is too inflated compared to its technological and social capabilities.
That is why, I'm still empty-handed. Promises are only that, and in Bitcoin,
there is no one to hold accountable. Technology can always get better with
well-remunerated engineers. But people and social problems don't get any better
because of technology.  As I have heard and experienced time and again: you
don't fix social problems with technology. Technology is just a tool, a powerful
tool, a valuable tool, but it is not the solution. Bitcoin doesn't fix it.

Why then care? Why write about it? Why the title? Because [Ben Hunt](https://www.epsilontheory.com/26878-2/) said it, and
it made so much sense. His words allowed me for the first time to see an
ideology I could start building upon.  Bitcoin may or may not be the
technological innovation of the century. Bitcoin may or may not become the
reserve asset of the world. It may or may not reprice all debt and bring a
deflationary and abundant future. Bitcoin may or may not open up worldwide trade
and disincentive violence.


## Today, Bitcoin is art. {#today-bitcoin-is-art-dot}

> Bitcoin is good art.  Bitcoin is elegant and beautiful fashion, sitting at the
> intersection of art and commerce. Most importantly, owning Bitcoin has been an
> authentic expression of identity, an extremely positive identity of autonomy,
> entrepreneurialism, and resistance to the Nudging State and the Nudging
> Oligarchy.
>
> There are no cash flows to art. There are no fundamentals to art. There is no
> “use case” to art.  There is only story. There is only narrative. There is only
> common knowledge – what everyone knows that everyone knows – about the value of
> art, common knowledge that emerges from our social interaction with story and
> narrative.
>
> --- [Ben Hunt](https://www.epsilontheory.com/in-praise-of-bitcoin/)

Let me materialize this analogy. For example, Pablo Picasso is an
internationally recognized artist, and his works are highly valued.  Despite
that, I don't like them, I would never buy them. If I receive any as a gift I
would not keep it. I would give it away to the first person who offers to take
it out of my hands.  In my profound ignorance, I have neither use nor
appreciation for it and so I could never efficiently trade with it. On the
contrary, I happen to like Monet's work, and I thought of it when creating this
post's image. My finances prohibit me from buying an original work of Monet,
despite that I do cherish replicas.  I have bought prints because I like them. I
understand what I like and want. Only then I can find an offer at a price point
which I'm happy to engage with.

That example doesn't explain anything from the quote. I know, and that is my
point. Art is not a thing that stands on its own, it is what you think of it.
At the same time, it is a movement, a community. It means belonging to the group
of people that like that art. It is about the people willing to be together on
that narrative.  It only makes sense to own an art piece when you like it. It is
a personal choice, and you decide how valuable it is, and to which of all the
imaginable offers you want to commit. Art has no value, it has a price, which
doesn't even represent a commercial value. The high price doesn't make it
universally desirable. And nothing prevents you from choosing imitations.

I agree Bitcoin is good art. It is a new form of art, one that I'm starting to
understand. I operate a Bitcoin node with varying levels of excitement and
curiosity. Yet, without coins of my own, there isn't much I can do with it. How
about a replica?  Yes! Absolutely! I run my node on the testnet too, I enjoy
that one at least. I have some test coins and move them around.  I enjoy the
experience, without suffering the monetary premium of the "real" network. You
should know, that the best things in life are free.

I find the testnet crucial and yet not discussed enough. It goes against the
pressuring salesman insisting you **buy bitcoin!** Nobody encourages you to **play
with bitcoin**. If you can't play with it at low stakes, you'll never learn to
play the game, you'll never know if you want the real deal. Bitcoin will miss
the opportunity to become great art. It is always **buy bitcoin** and the rest are
_sh\*tcoins_. The more they discourage you from any alternative, the more
suspicious I am of some slick salesman trying to sell you those _magic beans_
Jack.

Thus, I propose you play on the Bitcoin testnet, get some test coins, and move
them around.  Notice how dull money is when there is nothing to buy, and the
real network isn't any better. There are plenty of testnet faucets to get coins
for free, use them. The Bitcoin network had those faucets too, until the coins
felt scarce and started to appreciate in commercial value. The good thing is
that it is digital media and you can make as much as you want. You can always
start your network and make more coins. The digital world is not scarce, that is
the value of it.  That is what has brought so much wealth to the world, there is
enough for everybody and we can make more of it if we want.


## Is there any value to Bitcoin? {#is-there-any-value-to-bitcoin}

Yes, there is value to Bitcoin, but the value is not the price. I absolutely
appreciate that you can run your node and participate in the global consensus on
modest hardware. I value that I can own and control my balance on the network by
having my keys. Practiced otherwise it is akin to taking a selfie with an
artwork, which is fine too, you might be in only for the experience. I have
selfies in front of beautiful architectural buildings I will never own and that
is fine too. Recognize the fact that even if you can see it on your phone
screen, it doesn't mean you own it. Bitcoin is the art piece you can truly own
and verify, enjoying it requires you to own and verify. You can have a replica
too, those must offer you the same own and verify capabilities. You can then
pass them around too, and they can have value too. Because beauty/value is in
the eye of the beholder. Bitcoin is valuable if you believe it is.

~~Starting with this post you can endorse my posts with Testnet Lightning
Bitcoin.~~ There is nothing to buy, nothing to spend(except more of your time and
attention). There are no accounts to register, as should be in an open,
permissionless decentralized system. ~~Get some Testnet Lightning Bitcoin
immediately at [htcl.me](https://htlc.me), request a lightning invoice at the bottom of the page,
and pay. Watch it work! Maybe it won't be the future of payments, but I care
enough to have this in my future.~~ **Update <span class="timestamp-wrapper"><span class="timestamp">[2025-01-17 Fri]</span></span>**: I cancelled that [experiment]({{< relref "killing-the-lightning-experiment" >}}).

If you like it, go on, and get some on-chain test Bitcoin from a faucet. Run
your node, open a lightning channel, and give me some more _test_ endorsements.
Consider this as the honest way the ecosystem subsidizes your education, giving
you _playground_ money to practice.  If any technology is to gain any adoption
it needs a game where it is O.K. to _"lose"_ your funds. If any art is to gain
appreciation it needs to be in the hands and eyes of everyone.

P.S. If you want to thank me with _"real"_ Bitcoin, let me have a taste of the
_real_ deal and why it is worth it. Then send me an email, and we can arrange
that.
