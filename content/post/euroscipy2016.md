+++
title = "EuroScipy 2016"
authors = ["oscar"]
date = 2016-08-30
tags = ["sphinx-gallery"]
categories = ["talks"]
draft = false
projects = ["sphinx-gallery"]
+++

Sphinx-Gallery is an amazing project, and I got to talk about it in the
EuroScipy 2016. Which is great for the experience

Here I embed the presentation for convenience.

<div style="padding-bottom: 80%;position:relative;">
<iframe src="https://titan-c.github.io/sphinx-gallery-slides/" width="100%" height="100%" style="position:absolute;"></iframe>
</div>

You can find the full screen version of the presentation at:

<https://titan-c.github.io/sphinx-gallery-slides/>
