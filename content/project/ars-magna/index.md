+++
title = "Ars magna"
author = ["Dr. Óscar Nájera"]
date = 2023-11-19
lastmod = 2023-11-25T18:54:13+01:00
tags = ["blockchain"]
draft = false
images = ["images/ars-magna-logo-circle-shaded.svg"]
[meta_links]
  website = "https://arsmagna.xyz"
  "Youtube channel" = "https://www.youtube.com/channel/UCIGcTtEAq3aluoC5gRJjv5w"
  subprojects = "https://arsmagna.xyz/project/"
+++

Various projects in the blockchain space, particularly around the Cardano ecosystem.

It includes the Emacs extension `cardano.el`, a web service to edit transactions with
your web wallet and some documentation efforts.
