+++
title = "Delightful Dying"
author = ["Dr. Óscar Nájera"]
draft = false
[meta_links]
  website = "https://delightfuldying.com"
+++

An alternative take on life and dead planing. Live joyfully, leave
joyfully.
