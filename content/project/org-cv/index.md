+++
title = "Org-cv"
author = ["Dr. Óscar Nájera"]
lastmod = 2023-11-09T17:55:58+01:00
draft = false
[meta_links]
  website = "https://titan-c.gitlab.io/org-cv/"
  code = "https://gitlab.com/Titan-C/org-cv"
+++

Org-mode backend exporters for generating a Curriculum Vita using some
\\(\LaTeX\\) templates, or exporting to markdown for your hugo blog.
