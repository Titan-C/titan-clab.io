+++
title = "Sphinx-Gallery"
author = ["Dr. Óscar Nájera"]
draft = false
thumbnail_size = "262x150"
[meta_links]
  website = "https://sphinx-gallery.github.io/"
  code = "https://github.com/sphinx-gallery/sphinx-gallery"
+++

Sphinx-Gallery is a Sphinx extension that builds an HTML gallery of examples from any set of Python scripts.
