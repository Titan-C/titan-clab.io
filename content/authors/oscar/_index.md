---
lastmod: 2025-01-17T05:21:13+01:00
draft: false
avatar: "https://s.gravatar.com/avatar/271ea8b05668228229c638adb288ba27?s=64"
images: ["images/back.jpg", "https://s.gravatar.com/avatar/271ea8b05668228229c638adb288ba27?s=256"]
name: "Dr. Óscar Nájera"
role: "Software archeologist -- Recovering Physicist -- Dancer"
bio:
  "As a scientist I studied the physics of the very small quantum world. As a computer
  hacker I distill code. Software is eating the world, and less code means less
  errors, less problems. Millions of lines of legacy code demand attention and have
  to be understood and simplified for future reliable operation."

social:
  - icon_class: fas fa-envelope
    link: "/authors/oscar/#contact" # For a direct email link, use "mailto:test@example.org".
    label: email
  - icon_class: fab fa-github
    link: https://github.com/Titan-C
    label: github
  - icon_class: fab fa-gitlab
    link: https://gitlab.com/Titan-C
    label: gitlab
  - icon_class: fab fa-git-alt
    link: https://git.oscarnajera.com
    label: git-repository
  - icon_class: far fa-circle
    link: https://git.sr.ht/~titan-c/
    label: sourcehut
  - icon_class: fab fa-mastodon
    link: https://indieweb.social/@titan_c
    label: mastodon
  - icon_class: fab fa-linkedin
    link: https://www.linkedin.com/in/oscar-najera
    label: linkedin
  - icon_class: ai ai-google-scholar
    link: https://scholar.google.com/citations?user=usC8whUAAAAJ&hl=de
    label: Google scholar
  - icon_class: fab fa-instagram
    link: https://www.instagram.com/oscarandresnajera
    label: instagram
  - icon_class: fab fa-twitter
    link: https://twitter.com/najeraoscar1
    label: twitter
  - icon_class: fab fa-stack-overflow
    link: https://stackoverflow.com/users/8125789/titan-c
    label: stack overflow
---

> You're here because you know something. What you know you can't explain, but you
> feel it. You've felt it your entire life, that there's something wrong with the
> world.
>
> --- Morpheus, The Matrix


## Hello there! {#hello-there}

I go by the name **Óscar**. I used to be a physicist, but left academia many years
ago. Nowadays, I work as a software developer most of the time. I work
exclusively with Free/Libre and Open-Source Software(`FLOSS`) on Gnu/Linux systems.

Welcome to my turf on the internet, a unique place because it is mine. It isn't
inside a walled garden from a monopolistic Internet mega corporation. Here, in
the outskirts of the centralized internet, I'm free to do whatever I want. I set
the terms and conditions and share my sincere opinion.

I'm a profoundly curious person, hunting after every new thing. I studied
Physics because it was fun. It allowed me to explore the only universe I'll ever
life on. Despite its vastness, I found myself most captivated by the small
things, the electrons.

My journey unfolded when I moved to a foreign country(France) to pursue my PhD
in Strongly Correlated Electrons Systems. Those were amazing and crazy days. I
worked to understand how the interactions between electrons affect their
collective behavior.

Computers have been the most precious tool and weapon on my journey. Computers
aren't bound by the scarcity of nature, nor the constrains of shape and space.
They are a universal simulator and within them you can have anything you want
with zero materials cost. Everything is the same distance away, everything has
the same manufacturing process, you only have to put the work.

I'm incredibly happy that computers are accessible to everybody. Yet it saddens
me that we don't fight to keep ownership of them. Proprietary Software, Vendor
Locked Hardware, captured spaces on the internet(the cloud platforms) take the
endless possibilities away. You can only do what they tell you to do, and then
they make it mandatory to do as they say. I'm always fighting back.

Everything I know, originated by following my curiosity on the Internet. Which
is why I believe in keeping it open and free for everybody and the main reason I
use and contribute to Freedom and Privacy respecting software(also known as Free
Software or worse Open Source). That choice comes at great personal expense, I
self host my email, online backups, file shares and source version control
system. I have to do it.

Software is more than stuff, it is more than information, it is executable
knowledge. If having unread books lying around is a waste of opportunity, not
running software you control is a crime against your intellect. You are a
follower of what it does, and you have no idea of how to do it by yourself. That
makes it really dangerous.

When I'm not in front of the computer, you can find me dancing. I like to dance
West Coast Swing and Salsa the most. Dancing is the most amazing recreation form
it is Art and Sport, it is a challenging craftsmanship.

<!-- section break -->


## Contact {#contact}

You made it through that lengthy, text only presentation. Social media platforms
haven't hijacked your attention span. If you want to get in touch, the best way
of communication is by email. You can communicate securely with me using [GnuPG](https://en.wikipedia.org/wiki/GNU_Privacy_Guard)
(If you don't know about it, it is a good time to [learn](https://emailselfdefense.fsf.org/en/)). My key-ID has the
fingerprint:

`7D28 EBAE F208 A715 363D 3C7A BF54 14CD C5E4 73B3`

You can verify my linked identity to that key [on my keyoxide profile](https://keyoxide.org/7D28EBAEF208A715363D3C7ABF5414CDC5E473B3):

{{< figure src="/images/keyoxide_green.svg" alt="keyoxide logo" width="50" link="https://keyoxide.org/7D28EBAEF208A715363D3C7ABF5414CDC5E473B3" >}}

<!-- section break -->


## Experience {#experience}

<div class="cv-entry">

### Software Developer  {.cv-role}

<i></i>Byteplant GmbH
{.cv-host}
<i></i>Sep 2023 -- Present
{.cv-date}
<i></i>Germany
{.cv-location}

-   Data processing
-   DevOps
-   Software Development

</div>

<div class="cv-entry">

### Software Developer  {.cv-role}

<i></i>Tribe29 GmbH
{.cv-host}
<i></i>Sep 2018 -- Jul 2022
{.cv-date}
<i></i>Munich, Germany
{.cv-location}

-   Entirely responsible for customer communication, design and implementation
    -   Time series forecasting engine, Capacity management
    -   Time series retrieval mechanism, time-series pre-processor, and graph descriptors
    -   Grafana connector
-   Main contributor
    -   PDF reporting engine
    -   Front-end developer for user interface upgrade particularly: graphs and dashboards.
-   **Tools** : Python, C/C++, Javascript/TypeScript, Jenkins, Gerrit, Grafana

</div>

<div class="cv-entry">

### PhD candidate in Strongly Correlated Electron Systems  {.cv-role}

<i></i>Université Paris Saclay (Paris-Sud) -- Laboratoire de Physique des Solides
{.cv-host}
<i></i>Sep 2014 -- Dec 2017
{.cv-date}
<i></i>Orsay, France
{.cv-location}

-   Using the _Dynamical mean field theory_ I **implemented its highly complex
    algorithms and models** to generate, process and analyze data for a variety of
    experiments and theoretical simulations.
-   **Responsibilities** : Individual research, project planning, manage High
    Performance computing cluster, write documentation and dissertation
-   **Tools** : Python, C++, NumPy, SciPy, Sphinx, git, Travis-CI, Ansible

</div>
<!-- section break -->


## Certified Education {#certified-education}

<div class="cv-entry">

### CompTIA Data+ Certified  {.cv-role}

<i></i>CompTIA
{.cv-host}
<i></i>May 2023
{.cv-date}



</div>

<div class="cv-entry">

### ITIL® Foundation Certificate in IT Service Management  {.cv-role}

<i></i>AXELOS Global Best Practice
{.cv-host}
<i></i>Jan 2023
{.cv-date}



</div>

<div class="cv-entry">

### PhD in Strongly Correlated Electron Systems  {.cv-role}

<i></i>Université Paris Saclay (Paris-Sud) -- Laboratoire de Physique des Solides
{.cv-host}
<i></i>Sep 2014 -- Dec 2017
{.cv-date}
<i></i>Orsay, France
{.cv-location}

**Dissertation** Study of the dimer Hubbard Model within Dynamical Mean Field
Theory and its application to VO\\(\_2\\)

</div>

<div class="cv-entry">

### Master in Molecular Nano- bio-photonics (MONABIPHOT)  {.cv-role}

<i></i>École Normale Supérieure de Cachan
{.cv-host}
<i></i>Sep 2013 -- Aug 2014
{.cv-date}
<i></i>Cachan, France
{.cv-location}

**Mémoire** Study of spin-orbit effects in the Mott-Hubbard metal-insulator
 transition

</div>

<div class="cv-entry">

### Physics Diploma  {.cv-role}

<i></i>Escuela Politécnica Nacional
{.cv-host}
<i></i>Oct 2006 -- Aug 2012
{.cv-date}
<i></i>Quito, Ecuador
{.cv-location}

**Diploma Thesis** Estimation, by computer simulation, of the exchange energy
 dispersion between polar nano-regions in \\(Pb\_xBi\_4Ti\_{3+x}O\_{12+3x}; x=\\{2,3\\}\\)
 relaxor ferroelectrics

</div>

<div class="cv-entry">

### German Abitur  {.cv-role}

<i></i>Colegio Aleman Quito
{.cv-host}
<i></i>Oct 1997 -- Jun 2006
{.cv-date}
<i></i>Quito, Ecuador
{.cv-location}

-   German Abitur May 2006
-   Ecuadorian High School Diploma June 2005

</div>
<!-- section break -->


## Honors &amp; awards {#honors-and-awards}

<div class="cv-honor">

### PhD fellowship  {.cv-role}

<i></i>École Doctorale Physique en Île de France
{.cv-host}
<i></i>2014
{.cv-date}
<i></i>France
{.cv-location}


</div>

<div class="cv-honor">

### Master Scholarship  {.cv-role}

<i></i>Campus Paris-Saclay
{.cv-host}
<i></i>2013
{.cv-date}
<i></i>France
{.cv-location}


</div>

<div class="cv-honor">

### Danced for Ecuador in WDSF World Championship Standard  {.cv-role}

<i></i>WDSF
{.cv-host}
<i></i>2012
{.cv-date}
<i></i>Australia
{.cv-location}


</div>

<div class="cv-honor">

### PAD Preisträger  {.cv-role}

<i></i>Kultusminister Konferenz
{.cv-host}
<i></i>2003
{.cv-date}
<i></i>Germany
{.cv-location}


</div>
