+++
title = "Multiple crossovers and coherent states in a Mott-Peierls insulator"
authors = ["oscar", "M. Civelli", "V. Dobrosavljević", "M. J. Rozenberg"]
date = 2018-01-09
draft = false
thumbnail_size = "320x258 center webp"
[meta_links]
  DOI = "https://doi.org/10.1103/PhysRevB.97.045108"
+++

We consider the dimer Hubbard model within dynamical mean-field theory to study
the interplay and competition between Mott and Peierls physics. We describe the
various metal-insulator transition lines of the phase diagram and the breakdown
of the different solutions that occur along them. We focus on the specific issue
of the debated Mott-Peierls insulator crossover and describe the systematic
evolution of the electronic structure across the phase diagram. We found that at
low intradimer hopping, the emerging local magnetic moments can unbind above a
characteristic singlet temperature T\\(^\*\\). Upon increasing the interdimer
hopping, subtle changes occur in the electronic structure. Notably, we find
Hubbard bands of a mix character with coherent and incoherent excitations. We
argue that this state might be relevant for materials such as VO\\(\_2\\) and its
signatures may be observed in spectroscopic studies, and possibly through
pump-probe experiments.

Publication
: [Phys. Rev. B 97, 045108 (2018)](https://journals.aps.org/prb/abstract/10.1103/PhysRevB.97.045108)

arXiv
: [1707.09310](http://arxiv.org/abs/1707.09310)
