+++
title = "Capacity Management based on Monitoring data"
authors = ["oscar"]
date = 2020-08-01
draft = false
[meta_links]
  URL = "https://www.linux-magazin.de/ausgaben/2020/08/performance-vorhersage/"
+++

This article is in German.

In checkmk 1.6 I implemented the possibility to calculate forecasts over
monitored IT-resources in order to predict future utilization. This feature
was developed to allow on-premise service providers to independently
evaluate their resource utilization and plan their resource allocation.

<!--more-->

Publication
: [Linux Magazin 2020-08](https://www.linux-magazin.de/ausgaben/2020/08/performance-vorhersage/)
