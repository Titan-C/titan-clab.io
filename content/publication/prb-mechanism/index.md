+++
title = "Resolving the VO\\(_2\\) controversy: Mott mechanism dominates the insulator-to-metal transition"
authors = ["oscar", "M. Civelli", "V. Dobrosavljević", "M. J. Rozenberg"]
date = 2017-01-09
draft = false
thumbnail_size = "300x225 center webp"
[meta_links]
  DOI = "https://doi.org/10.1103/physrevb.95.035113"
+++

We consider a minimal model to investigate the metal-insulator transition in
VO\\(\_2\\). We adopt a Hubbard model with two orbitals per unit cell, which captures
the competition between Mott and singlet-dimer localization. We solve the model
within dynamical mean-field theory, characterizing in detail the metal-insulator
transition and finding new features in the electronic states. We compare our
results with available experimental data, obtaining good agreement in the
relevant model parameter range. Crucially, we can account for puzzling optical
conductivity data obtained within the hysteresis region, which we associate with
a metallic state characterized by a split heavy quasiparticle band. Our results
show that the thermal-driven insulator-to-metal transition in VO\\(\_2\\) is
compatible with a Mott electronic mechanism, providing fresh insight to a
long-standing "chicken-and-egg" debate and calling for further research of
"Mottronics" applications of this system.

Publication
: [Phys. Rev. B 95, 035113 (2017)](https://link.aps.org/doi/10.1103/PhysRevB.95.035113)

arXiv
: [1606.03157](http://arxiv.org/abs/1606.03157)
