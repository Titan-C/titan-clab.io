+++
title = "Imaging the nanoscale phase separation in vanadium dioxide thin films at terahertz frequencies"
authors = ["H. T. Stinson", "A. Sternbach", "oscar", "R. Jing", "A. S. Mcleod", "T. V. Slusar", "A. Mueller", "L. Anderegg", "H. T. Kim", "M. Rozenberg", "D. N. Basov"]
date = 2018-09-06
draft = false
thumbnail_size = "320x89 webp q80"
image_size = "685x190 webp"
[meta_links]
  DOI = "https://doi.org/10.1038/s41467-018-05998-5"
+++

Vanadium dioxide (VO\\(\_2\\)) is a material that undergoes an insulator–metal
transition upon heating above 340 K. It remains debated as to whether this
electronic transition is driven by a corresponding structural transition or by
strong electron–electron correlations. Here, we use apertureless scattering
near-field optical microscopy to compare nanoscale images of the transition in
VO\\(\_2\\) thin films acquired at both mid-infrared and terahertz frequencies, using
a home-built terahertz near-field microscope. We observe a much more gradual
transition when THz frequencies are utilized as a probe, in contrast to the
assumptions of a classical first-order phase transition. We discuss these
results in light of dynamical mean-field theory calculations of the dimer
Hubbard model recently applied to VO\\(\_2\\), which account for a continuous
temperature dependence of the optical response of the VO\\(\_2\\) in the insulating
state.

Publication
: [Nat. Commun vol. 9,1 3604. (2018)](https://doi.org/10.1038/s41467-018-05998-5)

arXiv
: [1711.05242](https://arxiv.org/abs/1711.05242)
