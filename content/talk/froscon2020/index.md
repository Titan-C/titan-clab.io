---
title: "My cloud, I rule my data and I rule how I communicate"
authors: ["oscar"]
date: 2020-08-23T10:00:00+02:00
tags: ["sovereign"]
draft: false
location: "Online"
meta_links:
  slides: "https://programm.froscon.de/2020/system/event_attachments/attachments/000/000/615/original/Slides.pdf"
  code: "https://github.com/Titan-C/sovereign/tree/froscon2020"
  video: "https://media.ccc.de/v/froscon2020-2598-my_cloud_i_rule_my_data_and_i_rule_how_i_communicate"
---

In this workshop I teach how to use the sovereign project. A set of Ansible
playbooks that you use to build and maintain you own personal cloud
entirely based on open source projects. It covers the basics, Email, online
storage sync, calendars, web hosting, git hosting, VPN.

<!--more-->

Event
: [Froscon2020](https://programm.froscon.de/2020/events/2598.html)

Location
: Online

Date
: 2020-08-23 10:00
