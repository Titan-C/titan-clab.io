+++
title = "Talks"
author = ["Dr. Óscar Nájera"]
draft = false
weight = 20
images = ["images/back.jpg"]
[cascade]
  image_size = "768x768 jpeg"
  thumbnail_size = "150x150 jpeg"
  metadata = true
+++
