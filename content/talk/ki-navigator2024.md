+++
title = "Regulating Technology"
authors = ["oscar"]
date = 2024-11-21T15:00:00+01:00
draft = false
location = "Nürnberg Convention Center"
thumbnail_size = "320x180 top webp"
images = ["images/talk/ki-navigator2024.jpg"]
date_end = 2024-11-21T15:45:00+01:00
publishDate = 2024-08-18T00:00:00+01:00
+++

A technology survives as long as it serves a solution in a better way than the problems it adds. Digitization has evolved from interesting to crucial in only a few decades. Artificial Intelligence shall experience an even faster adoption and challenges us to form opinions about things we didn't grow up with and don't always understand so well. That accompanies our move towards new regulation with panic, a rush for easy answers and inadvertence to complexities at hand.

Reflecting on the adoption of automobiles, we recognize that addressing the problems they brought carried responsibilities well beyond the automobile industry itself. We regulate safety standards, but can't stop accidents. Criminals use cars too, but that is a social and law enforcement problem not a mechanical engineering one. We have speed limits, traffic norms, but we don't build them into cars.

The digital space of impalpable nature makes itself all the harder to grasp and understand its consequences even when they manifest in the material world. Some common life practices are criminal in the digital space and at the same time unreasonable practices for everyday life have become common place in the digital space. Competition policies conflict with privacy policies, one promoting data sharing and the other blocking it. GDPR makes the internet unenjoyable rather than safe.

As a user, IT professional and innovator I'll walk you through the intersection of technology, regulation and society.

Event
: [KI Navigator 2024](https://ki-navigator.doag.org/de/home/)

Location
: Nürnberg Convention Center

    NürnbergMesse GmbH

    Messezentrum, 90471 Nürnberg

Date
: 2024-11-21 15:00-15:45 CET
