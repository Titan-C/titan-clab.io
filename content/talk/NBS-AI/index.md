+++
title = "Preventing the collapse of civilization"
authors = ["oscar"]
date = 2023-06-13T17:30:00+02:00
draft = false
location = "Siemens Campus Erlangen"
[caption]
  text = "Cartoon by Christopher Weyant"
  url = "https://www.cagle.com/chris-weyant/2023/05/ai-unleashed"
+++

This is NOT a talk about how AI could take over the world and enslave humans,
enough dramatic news, policies, and sci-fi novels tell that story.

But there are still many ramifications from our use of technology. From the very
way we record and distribute knowledge into society to how we actively use and
maintain available technologies.

This talk reviews our history and world view, and how we adapt to new
technologies as much as how we fail to provide maintenance to our technological
base. How missing to adopt a technology or how to support the active use of
established ones is more harmful than all the dangers and uncertainties that
arise from new technologies.

Event
: [Nürnberg Data Science Meetup #8](https://www.meetup.com/nuernberg-data-science/)

Location
: Siemens Campus Erlangen

    Siemenspromenade 1, 91058 Erlangen

Date
: 2023-06-13 17:30-20:30
