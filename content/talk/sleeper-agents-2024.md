+++
title = "Sleeper agents: training deceptive LLMs that persist through safety training"
authors = ["oscar"]
date = 2024-03-21T18:00:00+01:00
tags = ["AI"]
draft = false
location = "ZOLLHOF - Tech Incubator"
thumbnail_size = "320x212 webp"
images = ["images/talk/sleeper-agents.png"]
date_end = 2024-03-21T20:45:00+01:00
publishDate = 2024-03-15T18:45:00+01:00
+++

From political candidates to job-seekers, humans under selection pressure often
try to gain opportunities by hiding their true motivations. They present
themselves as more aligned with the expectations of their audience than they
actually are. If an AI system learned such a deceptive strategy, could we detect
it and remove it using current safety training techniques?

This talk is a review of the recent paper by _Hubinger, E., Denison, C., Mu, J.,
Lambert, M., Tong, M., MacDiarmid, M., Lanham, T., … (2024). Sleeper agents:
training deceptive llms that persist through safety training_. [arXiv: 2401.05566](https://arxiv.org/abs/2401.05566)

Event
: [Nürnberg Data Science Meetup #14](https://www.meetup.com/nuernberg-data-science/)

Location
: ZOLLHOF - Tech Incubator

    Zollhof 7, 90443 Nürnberg

Date
: 2023-03-21 17:30-20:30 CET
