+++
title = "English - The torture of the conqueror"
authors = ["oscar"]
date = 2019-11-16T10:00:00+01:00
tags = ["toastmasters"]
draft = false
location = "EuroAkademie Nürnberg"
thumbnail_size = "320x480 top webp"
image_size = "681x1024 top q90 webp"
[meta_links]
  video = "https://youtu.be/gdy5ioVN1Sc"
+++

This is a short humorous speech for the Wordstock Festival. I go over the
hurdles of learning English as a foreign language and all those language
quirks.

<!--more-->

Event
: [Wordstock](https://web.archive.org/web/20191128232347/https://www.toastmasters-bayern.org/en/wordstock-festival/)

Location
: EuroAkademie Nürnberg

    Karl-Grillenberger-Str. 3a, 90402 Nürnberg

Date
: 2019-11-16 10:00
