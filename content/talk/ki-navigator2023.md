+++
title = "Preventing the collapse of civilization again"
authors = ["oscar"]
date = 2023-11-23T16:00:00+01:00
draft = false
location = "Nürnberg Convention Center"
thumbnail_size = "320x180 webp"
images = ["images/talk/ki-navigator2023.png"]
date_end = 2023-11-23T16:45:00+01:00
publishDate = 2023-10-03T16:45:00+01:00
+++

This is NOT a talk about how AI could take over the world and enslave humans,
enough dramatic news, policies, and sci-fi novels tell that story.

But there are still many ramifications from our use of technology. From the very
way we record and distribute knowledge into society to how we actively use and
maintain available technologies.

This talk reviews our history and world view, and how we adapt to new
technologies as much as how we fail to provide maintenance to our technological
base. How missing to adopt a technology or how to support the active use of
established ones is more harmful than all the dangers and uncertainties that
arise from new technologies.

If you play with fire, you are gonna get burned. Yet there is no path to mastery without play.

Event
: [KI Navigator 2023](https://ki-navigator.doag.org/de/home/)

Location
: Nürnberg Convention Center

    NürnbergMesse GmbH, Messezentrum, 90471 Nürnberg

Date
: 2023-11-23 16:00-16:45 CET
