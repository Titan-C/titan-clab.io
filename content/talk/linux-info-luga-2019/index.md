+++
title = "Monitoring bees and more"
authors = ["oscar"]
date = 2019-04-06T15:15:00+02:00
tags = ["checkmk"]
draft = false
location = "Hochschule Augsburg, Fakultät für Informatik"
[meta_links]
  video = "https://youtu.be/OXTn0YFuwvE"
  slides = "https://www.luga.de/Aktionen/LIT-2019/Programm/system/event_attachments/attachments/000/000/044/original/Linuxtag_Augsburg.pdf"
+++

Held in German, this talk shows how to use Checkmk and about an interesting
use case about it flexibility when it is configured to monitory bee
colonies and their honey production.

<!--more-->

Event
: [18. Augsburger Linux-Infotag](https://www.luga.de/Aktionen/LIT-2019/)

Location
: Hochschule Augsburg, Fakultät für Informatik

    Friedbergerstr. 2, 86161 Augsburg

Date
: 2019-04-06 15:15
